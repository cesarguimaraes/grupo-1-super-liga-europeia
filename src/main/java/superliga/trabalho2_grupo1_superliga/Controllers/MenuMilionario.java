package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Milionario;
import Classes.dao.MilionarioDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;

import java.util.ArrayList;

public class MenuMilionario {
    @FXML
    private TextArea marcadorGeral;
    @FXML
    private TextArea marcadorEquipa;
    @FXML
    private TextArea amareloGeral;
    @FXML
    private TextArea amareloEquipa;
    @FXML
    private TextArea vermelhoGeral;
    @FXML
    private TextArea vermelhoEquipa;

    /**
     * Ao clicar no botão de Milionário inicia automaticamente com este método
     * Método para consultas da base de dados para as TextAreas
     */
    public void initialize(){
        //Aparecer jogadores relativos ao Clube
        MilionarioDAO daomilionario = new MilionarioDAO();
        ArrayList<Milionario> milionarioadd = new ArrayList<>();

        ObservableList<String> dataA = FXCollections.observableArrayList();
        //Adiciona no ObservableList os dados do jogador
        milionarioadd = daomilionario.findMarcadorGeral();
        for (Milionario m : milionarioadd) {
            dataA.add("Golos: "+ m.getQtd() + "| Clube: "+m.getNome_clubeMil()+"\n");
        }
        marcadorGeral.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());

        //CARTÕES AMARELOS
        dataA.clear();
        milionarioadd = daomilionario.findAmareloGeral();
        for (Milionario m : milionarioadd) {
            dataA.add("Amarelos: "+ m.getQtd() + " | Clube: "+m.getNome_clubeMil()+"\n");
        }
        amareloGeral.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());

        //CARTÕES VERMELHOS
        dataA.clear();
        milionarioadd = daomilionario.findVermelhoGeral();
        for (Milionario m : milionarioadd) {
            dataA.add("Vermelhos: "+ m.getQtd() + " | Clube: "+m.getNome_clubeMil()+"\n");
        }
        vermelhoGeral.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());

        //Marcador geral por Equipas
        dataA.clear();
        milionarioadd = daomilionario.findMarcadorEquipa();
        for (Milionario m : milionarioadd) {
            dataA.add(m.getNome_clubeMil() + "\n");
            dataA.add("> " + m.getNome_jogadorMil() + " | Golos: " + m.getQtd() +"\n");
        }
        marcadorEquipa.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());

        //Cartões amarelos por Equipas
        dataA.clear();
        milionarioadd = daomilionario.findAmareloEquipa();
        for (Milionario m : milionarioadd) {
            dataA.add(m.getNome_clubeMil() + "\n");
            dataA.add("> " + m.getNome_jogadorMil() + " | Amarelos: " + m.getQtd() +"\n");
        }
        amareloEquipa.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());

        //Cartões vermelhos por Equipas
        dataA.clear();
        milionarioadd = daomilionario.findVermelhoEquipa();
        for (Milionario m : milionarioadd) {
            dataA.add(m.getNome_clubeMil() + "\n");
            dataA.add("> " + m.getNome_jogadorMil() + " | Vermelhos: " + m.getQtd() +"\n");
        }
        vermelhoEquipa.setText(String.valueOf(dataA)
                .replace(",", "")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "")
                .trim());
    }

    /**
     * Botão para voltar oa menu principal
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("MenuPrincipal", "SUPERLIGA | Menu Principal");
    }
}
