package superliga.trabalho2_grupo1_superliga.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import superliga.trabalho2_grupo1_superliga.OpenApplication;

import java.io.IOException;

public class SwitchMenus {

    private static Stage stage = null;
    private static void openStage(String id, String title) throws Exception {
        //Resolver problema do null
        if (stage == null) stage = new Stage();

        Scene scene = new Scene(getLoader(id));
        Image image = new Image("https://cdn-icons-png.flaticon.com/512/53/53283.png");
        stage.getIcons().add(image);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.setTitle(title);
        stage.show();
    }

    public static void open(String id, String title) throws Exception {
        openStage(id, title);
    }

    public static void close(Scene scene) throws Exception {
        ((Stage) scene.getWindow()).close();
    }

    private static Parent getLoader(String path) throws Exception {
        try {
            return new FXMLLoader(OpenApplication.class.getResource(path + ".fxml")).load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
