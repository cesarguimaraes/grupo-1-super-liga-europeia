package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Jornada;
import Classes.dao.JornadaDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Esta classe extende os atributos e métedos do Controller AdminJogos
 * Apresenta todos os métedos necessários para esta classe funcionar
 */


public class AdminJornada {
    Jornada jorn =new Jornada();
    @FXML
    protected Button button;
    @FXML
    private ListView<String> admJornadalistview;
    private String jornadaatual;
    @FXML
    private Label jornadaLabel;

    JornadaDAO jDAO = new JornadaDAO();
    ArrayList<Jornada> jnor = new ArrayList<>();

    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize(){
        jnor = jDAO.findJornada();
       for (Jornada j : jnor) {
           admJornadalistview.getItems().add(

                   /*"Jornada " +*/ j.getDesc_jornada()
           );
       }
        admJornadalistview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                jornadaatual = admJornadalistview.getSelectionModel().getSelectedItem();
                jornadaLabel.setText(jornadaatual);
            }
        });
    }

    /**
     * Mudar para Admin>Jogos
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
        protected void switchToAdminJogos() throws Exception {
        AdminJogos adm1 = new AdminJogos();
           if (jornadaLabel.getText().isEmpty()){
                // Aviso quando algo corre mal
               Alert alert = new Alert(Alert.AlertType.WARNING);
               alert.setTitle("Aviso");
               alert.setHeaderText("Sem seleção");
               alert.setContentText("Selecione uma jornada da lista acima.");
               alert.showAndWait();
            } else {
               Stage window = (Stage) button.getScene().getWindow();
               window.close();
               //Passar informações para a Scene AdminJogos
               Controller.getInstance().setJornadaAtual(jornadaatual);
               SwitchMenus.open("AdminJogos", "SUPERLIGA >> Admin >> " + jornadaatual + " >> Jogos");
           }
        }

    /**
     * Butão para voltar para main menu
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("MenuPrincipal", "SUPERLIGA | Menu Principal");
    }



    public void onAdicionarJornada(ActionEvent actionEvent) throws Exception {
        JornadaDAO jornadi = new JornadaDAO();
        String addjoranada;

        TextInputDialog td = new TextInputDialog("Jornada");
        td.setTitle("Admin");
        td.setHeaderText("Inserir Jornada");
        td.setContentText("");
        Optional<String> result = td.showAndWait();
        result.ifPresent(name -> System.out.println("Jornada: " + name));
        addjoranada=result.get();

        String inserjorn = addjoranada;
        jorn.setDesc_jornada(inserjorn);
        jornadi.insertJornada(jorn);

        initialize();
    }

    public void onRemoverJornada(ActionEvent actionEvent) throws Exception {
        JornadaDAO jDAO = new JornadaDAO();
        ArrayList<Jornada> jnor = new ArrayList<>();
        Jornada jornada = new Jornada();

        String varapagar = String.valueOf(jornadaLabel.getText());
        jornada.setDesc_jornada(varapagar);
                jDAO.deleteJornada(jornada);

    }


    public String getJornadaatual(){
        return jornadaatual;
    }
}