package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Clube;
import Classes.Jogador;
import Classes.dao.ClubeDAO;
import Classes.dao.JogadorDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Arrays;

public class AdminClube {
    @FXML
    private Label label;

    @FXML
    private javafx.scene.control.TableView table;

    @FXML
    private TableColumn nomeJogador;
    @FXML
    private TableColumn numero;
    @FXML
    private TextField KeywordTextField;

    /**
     * Inicialização do controlador, vai alterar o texto da label para o clube atual!
     */
    public void initialize() {
        label.setText(Controller.getInstance().getClubeAtual());

        consultarJogadores();
    }

    /**
     *Consultar os jogadores e inserição nas tabelas laterais dos respetivo clube!
     */
    public void consultarJogadores(){
        //Aparecer jogadores relativos ao Clube
        JogadorDAO daojogador = new JogadorDAO();
        ClubeDAO daoClube = new ClubeDAO();
        ArrayList<Jogador> jogadoradd = new ArrayList<>();

        ObservableList<Classes.TableView> data = FXCollections.observableArrayList();
        //Adiciona no ObservableList os dados do jogador
        jogadoradd = (ArrayList<Jogador>) daojogador.findClubeA();
        for (Jogador j : jogadoradd) {
            data.add(new Classes.TableView(j.getNome_jogador(), j .getNumero_jogador()));
        }

        nomeJogador.setCellValueFactory(new PropertyValueFactory<Classes.TableView,String>("Nome"));
        numero.setCellValueFactory(new PropertyValueFactory<Classes.TableView,Integer>("Num"));
        //Limpa as tables para não aparecer erros de tabelas duplicadas
        table.getColumns().clear();

        table.setItems(data);
        table.getColumns().addAll(nomeJogador, numero);


        FilteredList<Classes.TableView> filteredData = new FilteredList<>(data, a -> true);

        KeywordTextField.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredData.setPredicate(TableView ->{
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
                    return true;
                }

                String searchKeyword = newValue.toLowerCase();

                if (TableView.getNome().toLowerCase().indexOf(searchKeyword) > -1) {
                    return true;
                } else {
                    String numberPlayer = Integer.toString(TableView.getNum());
                    return numberPlayer.indexOf(searchKeyword) > -1;
                }

            });
        });
        SortedList<Classes.TableView> sortedDataA = new SortedList<>(filteredData);

        sortedDataA.comparatorProperty().bind(table.comparatorProperty());

        table.setItems(sortedDataA);
    }

    /**
     * Butão para voltar para o menu anterior
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("AdminJornada", "SUPERLIGA >> Admin >> Jornada");
    }
}