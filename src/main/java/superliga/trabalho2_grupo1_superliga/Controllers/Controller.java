package superliga.trabalho2_grupo1_superliga.Controllers;

import javafx.scene.control.Button;

public class Controller {
    private int id_clube;

    private int id_jogador;

    private String clubeAtual;
    private String jogoAtual;
    private String jornadaAtual;
    private String ClubeA;
    private String ClubeB;

    private String nomeJogador;
    private int auxEstadioID;
    private String esconderBotoes;

    private final static Controller INSTANCE = new Controller();

    //Construtor privado
    private Controller() {
    }


    /**
     * Obter a instancia
     * @return instancia
     */
    public static Controller getInstance() {
        return INSTANCE;
    }

    /**
     * Devolve os botões escondidos
     *
     * @return botões escondidos
     */
    public String getEsconderBotoes() {
        return esconderBotoes;
    }

    /**
     * Modifica os botões escondidos
     *
     * @param esconderBotoes botões escondidos
     */
    public void setEsconderBotoes(String esconderBotoes) {
        this.esconderBotoes = esconderBotoes;
    }

    /**
     * Devolve o id do jogador
     *
     * @return id do jogador
     */
    public int getId_jogador() {
        return id_jogador;
    }

    /**
     * Modifica os ids dos jogadores
     *
     * @param id_jogador id dos jogadores
     */
    public void setId_jogador(int id_jogador) {
        this.id_jogador = id_jogador;
    }

    /**
     * Devolve o id do clube
     *
     * @return id do clube
     */
    public int getId_clube() {
        return id_clube;
    }

    /**
     * Modifica o id do clube
     *
     * @param id_clube id do clube
     */
    public void setId_clube(int id_clube) {
        this.id_clube = id_clube;
    }

    /**
     * Devolve o nome do jogador
     *
     * @return nome do jogador
     */
    public String getNomeJogador() {
        return nomeJogador;
    }

    /**
     * Modifica o nome do jogador
     *
     * @param nomeJogador nome do jogador
     */
    public void setNomeJogador(String nomeJogador) {
        this.nomeJogador = nomeJogador;
    }

    /**
     * Devolve o clube A
     *
     * @return Clube A
     */
    public String getClubeA() { return ClubeA; }

    /**
     * Devolve o clube B
     *
     * @return clube B
     */
    public String getClubeB() { return ClubeB; }

    /**
     * Modifica o clube A
     *
     * @param clubeA Clube A
     */
    public void setClubeA(String clubeA) {
        ClubeA = clubeA;
    }

    /**
     * Modifica o clube B
     *
     * @param clubeB clube B
     */
    public void setClubeB(String clubeB) {
        ClubeB = clubeB;
    }

    /**
     * Devolve o jogo atual
     *
     * @return jogo atual
     */
    public String getJogoAtual() {
        return jogoAtual;
    }

    /**
     * Modifica jogo atual
     *
     * @param jogoAtual jogo atual
     */
    public void setJogoAtual(String jogoAtual) {
        this.jogoAtual = jogoAtual;
    }

    /**
     * Devolve jornada atual
     *
     * @return jornada atual
     */
    public String getJornadaAtual() {
        return jornadaAtual;
    }

    /**
     * Devolve o clube atual
     *
     * @return clube atual
     */
    public String getClubeAtual() {return clubeAtual;}

    /**
     * Modifica clube atual
     *
     * @param clubeAtual clube atual
     */
    private void setJogoatual(String clubeAtual){this.clubeAtual = clubeAtual;};

    /**
     * Modifica jornada atual
     *
     * @param jornadaAtual jornada atual
     */
    public void setJornadaAtual(String jornadaAtual) {
        this.jornadaAtual = jornadaAtual;
    }

    /**
     * Devolve o id do estadio
     *
     * @return id estadio
     */
    public int getAuxEstadioID() {
        return auxEstadioID;
    }

    /**
     * Modifica o id do estadio
     *
     * @param auxEstadioID id do estadio
     */
    public void setAuxEstadioID(int auxEstadioID) {
        this.auxEstadioID = auxEstadioID;
    }
}

