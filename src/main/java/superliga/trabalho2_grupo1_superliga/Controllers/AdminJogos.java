package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Clube;
import Classes.Jogo;
import Classes.Jornada;
import Classes.dao.ClubeDAO;
import Classes.dao.JogoDAO;
import Classes.dao.JornadaDAO;
import Classes.dao.JornadaJogoDAO;
import Connect.ConnectionBd;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

/**
 * Classe pública que tem todos os atributos e métodos para o FXML AdminJogos
 */
public class AdminJogos  {
    private final static AdminJogos INSTANCE = new AdminJogos();

    //Construtor privado

    /**
     * Conexão com base de dados
     */
    public AdminJogos() {
        con = ConnectionBd.establishConnection();
    }

    /**
     * obter a instancia
     *
     * @return instacia
     */
    public static AdminJogos getInstance() {
        return INSTANCE;
    }
    private Connection con = null;


    @FXML
    protected Button button;
    @FXML
    private ListView<String> admJogolistview;
    Jornada jorn;
    @FXML
    private Label jogoLabel;
    @FXML
    private Label labelJogos;

    String jogoatual;

    JogoDAO daojogo = new JogoDAO();
    ArrayList<Jogo> jogoadd = new ArrayList<>();

    ClubeDAO daoclube = new ClubeDAO();
    ArrayList<Clube> clubeadd = new ArrayList<>();


    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize() {
        int id = 0;
        jogoadd = daojogo.findJogo();
        for (Jogo j : jogoadd) {
            admJogolistview.getItems().add(
                    j.getDesc_jogo() +" > 1."+ j.getNome_clubeA() + " | 2." + j.getNome_clubeB()
            );
        }
        admJogolistview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                jogoatual=admJogolistview.getSelectionModel().getSelectedItem();
                jogoLabel.setText(jogoatual);
            }

        });
        labelJogos.setText(Controller.getInstance().getJornadaAtual());
    }


    /**
     * Mudar para o menu Admin>Eventos>Jogo
     *
     * @param event Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToAdminEventosJogo(ActionEvent event) throws Exception {

        if (jogoLabel.getText().isEmpty()){
            /**
             * Aviso quando algo corre mal
             */
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione uma jornada da lista acima.");
            alert.showAndWait();
        } else {
            Stage window = (Stage) button.getScene().getWindow();
            window.close();
            //Passar informações para a Scene AdminEventosJogo
            Controller.getInstance().setJogoAtual(jogoatual);
            SwitchMenus.open("AdminEventosJogo", "SUPERLIGA >> Admin >> " + Controller.getInstance().getJornadaAtual() + " | " + jogoatual + " >> Eventos do jogo");
        }
    }

    public String getJogoatual(){
        return this.jogoatual;
    }

    public void setlabelJogos(String jornadaatual) {
        labelJogos.setText(jornadaatual);
    }

Jogo jogo =new Jogo();
    JornadaJogoDAO jornjog =new JornadaJogoDAO();


    /**
     * butão para adicionar jogo
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void onAdicionarJogo(ActionEvent actionEvent) throws Exception {
        ArrayList<Jogo> array = new ArrayList<>();
        JogoDAO jog = new JogoDAO();
        String addjogo1, addjogo2, addjogo3, addjogo4, addjogo5, addjogo6, addjogo7;

        TextInputDialog td = new TextInputDialog("Jogo");
        td.setTitle("Admin");
        td.setHeaderText("Inserir Jogo");
        td.setContentText("");
        Optional<String> result1 = td.showAndWait();
        td.setHeaderText("Inserir id clube a");
        Optional<String> result2 = td.showAndWait();
        td.setHeaderText("Inserir id clube b");
        Optional<String> result3 = td.showAndWait();
        td.setHeaderText("Inserir id estadio");
        Optional<String> result4 = td.showAndWait();
        td.setHeaderText("Inserir nome clube a");
        Optional<String> result5 = td.showAndWait();
        td.setHeaderText("Inserir nome clube b");
        Optional<String> result6 = td.showAndWait();
        td.setHeaderText("Inserir id do jogo");
        Optional<String> result7 = td.showAndWait();
        addjogo1=result1.get();
        addjogo2=result2.get();
        addjogo3=result3.get();
        addjogo4=result4.get();
        addjogo5=result5.get();
        addjogo6=result6.get();
        addjogo7=result7.get();

        String inserjog = addjogo1;
        jogo.setDesc_jogo(inserjog);

         inserjog = addjogo2;
        jogo.setId_clubeA(Integer.parseInt(inserjog));

        inserjog = addjogo3;
        jogo.setId_clubeB(Integer.parseInt(inserjog));

        inserjog = addjogo4;
        jogo.setId_estadio(Integer.parseInt(inserjog));

        inserjog = addjogo5;
        jogo.setNome_clubeA(String.valueOf(inserjog));

        inserjog = addjogo6;
        jogo.setNome_clubeB(String.valueOf(inserjog));

        inserjog = addjogo7;
        jornjog.insertJornadaJogo(1, Integer.parseInt(inserjog));

        jog.insertJogo(jogo);
       // jogo.setDesc_jogo();
    }

    /**
     * butão para remover jogo
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void onRemoverJogo(ActionEvent actionEvent) throws Exception {
        JogoDAO jogoDAO = new JogoDAO();
        Jogo jog= new Jogo();
        int coiza;

        String jogo = "0";
        String jornadajogo = "0";
        String jornadajogo2 = "0";

        jogo =  jogoatual.substring(0, jogoatual.indexOf(">")-1);
        jornadajogo =  jogoatual.substring(0, jogoatual.indexOf(">")-1);
        jornadajogo2=jornadajogo;
        jornadajogo2= jornadajogo2.replace("Jogo ","");

        System.out.println(jornadajogo);
        System.out.println(jornadajogo2);

        jog.setId_jogo(Integer.parseInt(jornadajogo));
        jogoDAO.deleteJogo(jog);
    }

    /**
     * Ao clicar no botão, volta para o menu Admin > Jornada
     *
     * @param actionEvent
     * @throws Exception
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("AdminJornada", "SUPERLIGA >> Admin >> Jornada");
    }

}
