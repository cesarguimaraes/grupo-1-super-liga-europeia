package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Jornada;
import Classes.dao.JornadaDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Esta classe extende os atributos e métedos do Controller AdminJogos
 * Apresenta todos os métedos necessários para esta classe funcionar
 */
public class OperadorJornada {
    Button but;
    @FXML
    Label label;
    JornadaDAO dao= new JornadaDAO();
    Jornada jorn =new Jornada();
    @FXML
    protected Button button;
    @FXML
    private ListView<String> opJornadalistview;
    private String jornadaatual;
    @FXML
    private Label jornadaLabel;

    JornadaDAO jDAO = new JornadaDAO();
    ArrayList<Jornada> jnor = new ArrayList<>();

    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize(){
        jnor = jDAO.findJornada();
       for (Jornada j : jnor) {
           opJornadalistview.getItems().add(

                   /*"Jornada " +*/ j.getDesc_jornada()
           );
       }
        opJornadalistview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                jornadaatual = opJornadalistview.getSelectionModel().getSelectedItem();
                jornadaLabel.setText(jornadaatual);
            }
        });
    }

    /**
     * Botão para trocar para o menu Operador > jogos
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
        protected void switchToOpJogos() throws Exception {
           if (jornadaLabel.getText().isEmpty()){
               /**
                * Aviso quando algo corre mal
                */
               Alert alert = new Alert(Alert.AlertType.WARNING);
               alert.setTitle("Aviso");
               alert.setHeaderText("Sem seleção");
               alert.setContentText("Selecione uma jornada da lista acima.");
               alert.showAndWait();
            } else {
               Stage window = (Stage) button.getScene().getWindow();
               window.close();
               //Passar informações para a Scene OperadorJogos
               Controller.getInstance().setJornadaAtual(jornadaatual);
               SwitchMenus.open("OperadorJogos", "SUPERLIGA >> Operador >> " + jornadaLabel.getText() + " >> Jogos");
           }
        }

    /**
     * Botão para voltar para o menu principal
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("MenuPrincipal", "SUPERLIGA | Menu Principal");
    }

    /**
     * Adicionar nova jornada
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void onAdicionarJornada(ActionEvent actionEvent) throws Exception {

        label.setText("deu");
        JornadaDAO jornadi=null;
        jornadi.insertJornada(jorn);
    }

    /**
     * Devolve a jornada atual
     *
     * @return jornada atual
     */
    public String getJornadaatual(){
        return jornadaatual;
    }
}