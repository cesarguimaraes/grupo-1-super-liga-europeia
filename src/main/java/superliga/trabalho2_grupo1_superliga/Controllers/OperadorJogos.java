package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.Clube;
import Classes.Jogo;
import Classes.Jornada;
import Classes.dao.ClubeDAO;
import Classes.dao.JogoDAO;
import Connect.ConnectionBd;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Esta classe extende os atributos e métedos do Controller AdminEventosJogo
 * Apresenta todos os métedos necessários para esta classe funcionar
 */
public class OperadorJogos {
    private Connection con = null;

    public OperadorJogos() {
        con = ConnectionBd.establishConnection();
    }

    @FXML
    protected Button button;
    @FXML
    private ListView<String> opJogolistview;
    Jornada jorn;
    @FXML
    private Label jogoLabel;
    @FXML
    private Label labelJogos;

    String jogoatual;

    JogoDAO daojogo = new JogoDAO();
    ArrayList<Jogo> jogoadd = new ArrayList<>();

    ClubeDAO daoclube = new ClubeDAO();
    ArrayList<Clube> clubeadd = new ArrayList<>();

    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize(){
        int id=0;
        jogoadd = daojogo.findJogo( );
        for (Jogo j : jogoadd) {

            opJogolistview.getItems().add(
                    j.getDesc_jogo() +" > 1."+ j.getNome_clubeA() + " | 2." + j.getNome_clubeB()
            );
        }

        opJogolistview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                jogoatual=opJogolistview.getSelectionModel().getSelectedItem();
                jogoLabel.setText(jogoatual);
            }
        });
        labelJogos.setText(Controller.getInstance().getJornadaAtual());
    }

    /**
     * Botão para mudar para o meunu operador > Eventos > Jogo
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToOpEventosJogo() throws Exception {
        if (jogoLabel.getText().isEmpty()){
            /**
             * Aviso quando algo corre mal
             */
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione uma jornada da lista acima.");
            alert.showAndWait();
        } else {
            Stage window = (Stage) button.getScene().getWindow();
            window.close();
            //Passar informações para a Scene OperadorEventosJogo
            Controller.getInstance().setJogoAtual(jogoatual);
            SwitchMenus.open("OperadorEventosJogo", "SUPERLIGA >> Operador >> " + Controller.getInstance().getJornadaAtual() + " | " + jogoatual + " >> Eventos do jogo");
        }
    }

    /**
     * Obter texto da label jogos
     *
     * @return texto da label jogos
     */
    public String getlabelJogos(){
        return labelJogos.getText();
    }

    /**
     * Modificar texto da label jogos
     *
     * @param jornadaatual alterar texto da jornada atual
     */
    public void setlabelJogos(String jornadaatual) {
        labelJogos.setText(jornadaatual);
    }

    /**
     * Botão para voltar ao menu Operador > Jornada
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("OperadorJornada", "SUPERLIGA >> Operador >> Jornada");
    }
}
