package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.*;
import Classes.TableView;
import Classes.dao.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe pública com todos os métodos desta classe
 */
public class OperadorEventosJogo {
    @FXML
    private Label labelTudo;
    @FXML
    private Label estadioID;
    @FXML
    private Label clube;
    @FXML
    private TextArea pais;
    @FXML
    private TextArea cidade;
    @FXML
    private TextArea estadio;
    @FXML
    private Button buttonClubeA;
    @FXML
    private Button buttonClubeB;
    @FXML
    private javafx.scene.control.TableView tableViewA;
    @FXML
    private javafx.scene.control.TableView tableViewB;
    @FXML
    private TableColumn nomeJogadorA;
    @FXML
    private TableColumn nomeJogadorB;
    @FXML
    private TableColumn numeroA;
    @FXML
    private TableColumn numeroB;
    @FXML
    private TextField KeywordTextFieldA;
    @FXML
    private TextField KeywordTextFieldB;
    @FXML
    private ListView listViewEventos;
    ObservableList listaEventos = FXCollections.observableArrayList();

    /**
     * Ao chamar a classe pública este método inicia com a mesma
     */
    public void initialize(){

        labelTudo.setText(Controller.getInstance().getJogoAtual());

        //Remove qualquer (número , > . [(array)] jogo e espaços
        String clubeA = Arrays.toString(Controller.getInstance().getJogoAtual()
                        .split("[0-9]"))
                .replace(",", "")
                .replace(">", "")
                .replace(".", "")
                .replace("[", "")
                .replace("]", "")
                .replace("jogo", "").trim();
        String clubeB = Arrays.toString(Controller.getInstance().getJogoAtual()
                        .split("[0-9]"))
                .replace(",", "")
                .replace(">", "")
                .replace(".", "")
                .replace("[", "")
                .replace("]", "")
                .replace("jogo", "");
        //Apaga para a frente de |
        clubeA =  clubeA.substring(0, clubeA.indexOf("|")-1);
        //Apaga para trás de |
        clubeB =  clubeB.substring(clubeB.lastIndexOf("| ")+1);
        buttonClubeA.setText(clubeA.trim());
        buttonClubeB.setText(clubeB.trim());
        Controller.getInstance().setClubeA(clubeA.trim());
        Controller.getInstance().setClubeB(clubeB.trim());
        //Chamar métodos
        consultarJogadores();
        consultarEventos();
        consultarLocalizacao();
    }

    /**
     * Consultar jogadores na TableView do respetivo Clube
     */
    public void consultarJogadores(){
        //Aparecer jogadores relativos ao Clube
        JogadorDAO daojogador = new JogadorDAO();
        ArrayList<Jogador> jogadoradd = new ArrayList<>();

        ObservableList<TableView> dataA = FXCollections.observableArrayList();
        ObservableList<TableView> dataB = FXCollections.observableArrayList();
        //Adiciona no ObservableList os dados do jogador
        jogadoradd = (ArrayList<Jogador>) daojogador.findClubeA();
        for (Jogador j : jogadoradd) {
            dataA.add(new TableView(j.getNome_jogador(), j.getNumero_jogador()));
        }
        jogadoradd = (ArrayList<Jogador>) daojogador.findClubeB();
        for (Jogador j : jogadoradd) {
            dataB.add(new TableView(j.getNome_jogador(), j.getNumero_jogador()));
        }

        nomeJogadorA.setCellValueFactory(new PropertyValueFactory<TableView, String>("Nome"));
        numeroA.setCellValueFactory(new PropertyValueFactory<TableView, Integer>("Num"));
        nomeJogadorB.setCellValueFactory(new PropertyValueFactory<TableView, String>("Nome"));
        numeroB.setCellValueFactory(new PropertyValueFactory<TableView, Integer>("Num"));
        //Limpa as tables para não aparecer erros de tabelas duplicadas
        tableViewA.getColumns().clear();
        tableViewB.getColumns().clear();

        tableViewA.setItems(dataA);
        tableViewA.getColumns().addAll(nomeJogadorA, numeroA);
        tableViewB.setItems(dataB);
        tableViewB.getColumns().addAll(nomeJogadorB, numeroB);

        FilteredList<TableView> filteredDataA = new FilteredList<>(dataA, a -> true);

        KeywordTextFieldA.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredDataA.setPredicate(TableView -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
                    return true;
                }

                String searchKeywordA = newValue.toLowerCase();

                if (TableView.getNome().toLowerCase().indexOf(searchKeywordA) > -1) {
                    return true;
                } else {
                    String numberPlayerA = Integer.toString(TableView.getNum());
                    return numberPlayerA.indexOf(searchKeywordA) > -1;
                }

            });
        });
        SortedList<TableView> sortedDataA = new SortedList<>(filteredDataA);

        sortedDataA.comparatorProperty().bind(tableViewA.comparatorProperty());

        tableViewA.setItems(sortedDataA);


        FilteredList<TableView> filteredDataB = new FilteredList<>(dataB, b -> true);

        KeywordTextFieldB.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredDataB.setPredicate(TableView -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
                    return true;
                }

                String searchKeywordB = newValue.toLowerCase();

                if (TableView.getNome().toLowerCase().indexOf(searchKeywordB) > -1) {
                    return true;
                } else {
                    String numberPlayerB = Integer.toString(TableView.getNum());
                    return numberPlayerB.indexOf(searchKeywordB) > -1;
                }

            });
        });
        SortedList<TableView> sortedDataB = new SortedList<>(filteredDataB);

        sortedDataB.comparatorProperty().bind(tableViewB.comparatorProperty());

        tableViewB.setItems(sortedDataB);
    }

    /**
     * Consultar a localização do jogo no TABPANE
     */
    private void consultarLocalizacao() {
        EstadioDAO daoestadio = new EstadioDAO();
        ArrayList<Estadio> estadioadd = new ArrayList<>();
        JogoDAO daojogo = new JogoDAO();
        ArrayList<Jogo> jogoadd = new ArrayList<>();
        ClubeDAO daoclube = new ClubeDAO();
        ArrayList<Clube> clubeadd = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        //Seta dados do local do jogo nas labels
        jogoadd = daojogo.findLocalID();
        for (Jogo e : jogoadd){
            estadioID.setText(String.valueOf(e.getId_estadio()));
        }
        Controller.getInstance().setAuxEstadioID(Integer.parseInt(estadioID.getText()));

        clubeadd = daoclube.findLocalClube();
        for (Clube c : clubeadd){
            clube.setText(c.getNome_clube());
        }
        estadioadd = daoestadio.findLocalEstadio();
        for (Estadio e : estadioadd){
            pais.setText(e.getPais());
            cidade.setText(e.getCidade());
            estadio.setText(e.getEstadio());
        }
    }
    /**
     * Consultar eventos ao respetivo jogo no TABPANE
     */
    public void consultarEventos() {
        EventoDAO daoevento = new EventoDAO();
        ArrayList<Evento> eventoadd;
        JogadorDAO daojogador = new JogadorDAO();
        ArrayList<Jogador> jogadoradd;
        ClubeDAO daoclube = new ClubeDAO();
        ArrayList<Clube> clubeadd;

        eventoadd = (ArrayList<Evento>) daoevento.findEventos();
        for (Evento e : eventoadd) {
            String nome_clube = "";
            String nome_jogador = "";

            jogadoradd = (ArrayList<Jogador>) daojogador.findNomeJogador(e.getId_jogador());
            for (Jogador j : jogadoradd) {
                nome_jogador = j.getNome_jogador();
            }
            clubeadd = (ArrayList<Clube>) daoclube.findNomeClubeEvento(e.getId_clube());
            for (Clube c : clubeadd) {
                nome_clube = c.getNome_clube();
            }

            listaEventos.add("[ADMIN -> EVENTO]: Administrador adicionou um " + e.getDesc_evento() + " ao jogador "
                    + nome_jogador + " do Clube: " + nome_clube + " | MINUTO " + e.getMinuto());
        }

        listViewEventos.setItems(listaEventos);
    }

    /**
     * Adicionar eventos à TableViewA
     */
    public void adicionarEventoA(){
        Integer index = -1;
        index = tableViewA.getSelectionModel().getSelectedIndex();

        if (index > -1){
            //Recebo o nome do jogador da TableView selecionado
            TableView jogador = (TableView) tableViewA.getSelectionModel().getSelectedItem();
            Controller.getInstance().setNomeJogador(jogador.getNome());
            //Abre um popup para adicionar o evento
            popUpEvento();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
            alert.showAndWait();
        }
    }

    /**
     * Adicionar eventos à TableViewB
     */
    public void adicionarEventoB(){
        Integer index = -1;
        index = tableViewB.getSelectionModel().getSelectedIndex();

        if (index > -1){
            //Recebo o nome do jogador da TableView selecionado
            TableView jogador = (TableView) tableViewB.getSelectionModel().getSelectedItem();
            Controller.getInstance().setNomeJogador(jogador.getNome());
            //Abre um popup para adicionar o evento
            popUpEvento();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
            alert.showAndWait();
        }
    }

    /**
     * PopUp chamado ao clicar no botão de adicionar o evento
     */
    public void popUpEvento(){
        String[] items = {"Golo", "Cartão amarelo", "Cartão vermelho"};
        JComboBox<String> combo = new JComboBox<>(items);
        JTextField min = new JTextField("");
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(combo);
        panel.add(new JLabel("Minuto:"));
        panel.add(min);
        int result = JOptionPane.showConfirmDialog(null, panel, "Adicionar evento",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        //Se a pessoa clicar em OK no popup
        if (result == JOptionPane.OK_OPTION) {
            String desc = "";
            int minuto = 0;
            int id_jogo = 0;
            int id_clube = 0;
            int id_jogador = 0;

            Jogador jogador1 =new Jogador();
            JogadorDAO daojogador = new JogadorDAO();
            ArrayList<Jogador> jogadoradd = new ArrayList<>();
            //Busco o ID do jogador pelo nome do jogador recebido
            jogadoradd = (ArrayList<Jogador>) daojogador.findIDJogador();
            for (Jogador j : jogadoradd){
                id_clube = j.getId_clube();
                id_jogador = j.getId_jogador();
                desc = (String) combo.getSelectedItem();
                minuto = Integer.parseInt(min.getText());
            }
            //Busca o ID do jogo com a desc_jogo
            JogoDAO daojogo = new JogoDAO();
            ArrayList<Jogo> jogoadd = new ArrayList<>();
            Jogo jogo =new Jogo();
            jogoadd = daojogo.findIDJogo();
            for (Jogo j : jogoadd){
                id_jogo = j.getId_jogo();
            }

            //Insere o evento na base de dados
            Evento evento1 =new Evento();
            EventoDAO daoevento = new EventoDAO();
            evento1.setDesc_evento(desc);
            evento1.setMinuto(minuto);
            evento1.setId_jogo(id_jogo);
            evento1.setId_clube(id_clube);
            evento1.setId_jogador(id_jogador);
            daoevento.insertEvento(evento1);
            listaEventos.add("[ADMIN -> EVENTO]: Administrador adicionou um "+desc+" ao jogador "+Controller.getInstance().getNomeJogador()
                    +" do Clube "+Controller.getInstance().getClubeA()+" | MINUTO "+minuto);
            listViewEventos.setItems(listaEventos);
        } else {
            System.out.println("Cancelado");
        }
    }

    public void onbuttonClubeA(ActionEvent event)throws Exception{
        Stage window = (Stage) buttonClubeA.getScene().getWindow();
        window.close();
        SwitchMenus.open("OperadorClube", "SUPERLIGA >> Operador >> Clube");

    }
    @FXML
    public void onbuttonClubeB(ActionEvent event)throws Exception{
        Stage window = (Stage) buttonClubeB.getScene().getWindow();
        window.close();
        SwitchMenus.open("OperadorClube", "SUPERLIGA >> Operador >> Clube");

    }

    /**
     * Volta para o Scene OperadorJornada
     *
     * @param actionEvent
     * @throws Exception
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("OperadorJornada", "SUPERLIGA >> Operador >> Jornada");
    }
}
