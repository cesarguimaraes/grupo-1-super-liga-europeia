package superliga.trabalho2_grupo1_superliga.Controllers;

import Classes.TableView;
import Classes.*;
import Classes.dao.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Classe pública que tem todos os atributos e métodos para o FXML AdminEventosJogo
 */
public class AdminEventosJogo {
    @FXML
    protected Button button;

    @FXML
    protected ComboBox<String> comboBoxEstadio;
    @FXML
    private Label labelTudo;
    @FXML
    private Label estadioID;
    @FXML
    private Button buttonClubeA;
    @FXML
    private Button buttonClubeB;

    @FXML
    private Label clube;
    @FXML
    private TextArea pais;
    @FXML
    private TextArea cidade;
    @FXML
    private TextArea estadio;
    @FXML
    private ListView listViewEventos;
    @FXML
    private javafx.scene.control.TableView tableViewA;
    @FXML
    private javafx.scene.control.TableView tableViewB;
    @FXML
    private TableColumn nomeJogadorA;
    @FXML
    private TableColumn nomeJogadorB;
    @FXML
    private TableColumn numeroA;
    @FXML
    private TableColumn numeroB;
    @FXML
    private TextField KeywordTextFieldA;
    @FXML
    private TextField KeywordTextFieldB;

    ObservableList listaEventos = FXCollections.observableArrayList();
    //ComboBox da localização do jogo
    ObservableList<String> obsClubes = FXCollections.observableArrayList();

    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize() {
        labelTudo.setText(Controller.getInstance().getJogoAtual());

        //Remove qualquer (número , > . [(array)] jogo e espaços
        String clubeA = Arrays.toString(Controller.getInstance().getJogoAtual()
                        .split("[0-9]"))
                .replace(",", "")
                .replace(">", "")
                .replace(".", "")
                .replace("[", "")
                .replace("]", "")
                .replace("jogo", "");
        String clubeB = Arrays.toString(Controller.getInstance().getJogoAtual()
                        .split("[0-9]"))
                .replace(",", "")
                .replace(">", "")
                .replace(".", "")
                .replace("[", "")
                .replace("]", "")
                .replace("jogo", "");
        //Apaga para a frente de |
        clubeA = clubeA.substring(0, clubeA.indexOf("|") - 1);
        //Apaga para trás de |
        clubeB = clubeB.substring(clubeB.lastIndexOf("| ") + 1);
        buttonClubeA.setText(clubeA.trim());
        buttonClubeB.setText(clubeB.trim());
        //Pode ser usado em qualquer lugar
        Controller.getInstance().setClubeA(clubeA.trim());
        Controller.getInstance().setClubeB(clubeB.trim());
        //Chamar métodos
        consultarJogadores();
        consultarEventos();
        consultarLocalizacao();
    }

    /**
     * Consultar jogadores na TableView do respetivo Clube
     */
    public void consultarJogadores(){
        //Aparecer jogadores relativos ao Clube
        JogadorDAO daojogador = new JogadorDAO();
        ArrayList<Jogador> jogadoradd = new ArrayList<>();

        ObservableList<TableView> dataA = FXCollections.observableArrayList();
        ObservableList<TableView> dataB = FXCollections.observableArrayList();
        //Adiciona no ObservableList os dados do jogador
        jogadoradd = (ArrayList<Jogador>) daojogador.findClubeA();
        for (Jogador j : jogadoradd) {
            dataA.add(new TableView(j.getNome_jogador(), j.getNumero_jogador()));
        }
        jogadoradd = (ArrayList<Jogador>) daojogador.findClubeB();
        for (Jogador j : jogadoradd) {
            dataB.add(new TableView(j.getNome_jogador(), j.getNumero_jogador()));
        }

        nomeJogadorA.setCellValueFactory(new PropertyValueFactory<TableView,String>("Nome"));
        numeroA.setCellValueFactory(new PropertyValueFactory<TableView,Integer>("Num"));
        nomeJogadorB.setCellValueFactory(new PropertyValueFactory<TableView,String>("Nome"));
        numeroB.setCellValueFactory(new PropertyValueFactory<TableView,Integer>("Num"));
        //Limpa as tables para não aparecer erros de tabelas duplicadas
        tableViewA.getColumns().clear();
        tableViewB.getColumns().clear();

        tableViewA.setItems(dataA);
        tableViewA.getColumns().addAll(nomeJogadorA, numeroA);
        tableViewB.setItems(dataB);
        tableViewB.getColumns().addAll(nomeJogadorB, numeroB);

        FilteredList<TableView> filteredDataA = new FilteredList<>(dataA, a -> true);

        KeywordTextFieldA.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredDataA.setPredicate(TableView ->{
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
                    return true;
                }

                String searchKeywordA = newValue.toLowerCase();

                if (TableView.getNome().toLowerCase().indexOf(searchKeywordA) > -1) {
                    return true;
                } else {
                    String numberPlayerA = Integer.toString(TableView.getNum());
                    return numberPlayerA.indexOf(searchKeywordA) > -1;
                }

            });
        });
        SortedList<TableView> sortedDataA = new SortedList<>(filteredDataA);

        sortedDataA.comparatorProperty().bind(tableViewA.comparatorProperty());

        tableViewA.setItems(sortedDataA);


        FilteredList<TableView> filteredDataB = new FilteredList<>(dataB, b -> true);

        KeywordTextFieldB.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredDataB.setPredicate(TableView ->{
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
                    return true;
                }

                String searchKeywordB = newValue.toLowerCase();

                if (TableView.getNome().toLowerCase().indexOf(searchKeywordB) > -1) {
                    return true;
                } else {
                    String numberPlayerB = Integer.toString(TableView.getNum());
                    return numberPlayerB.indexOf(searchKeywordB) > -1;
                }

            });
        });
        SortedList<TableView> sortedDataB = new SortedList<>(filteredDataB);

        sortedDataB.comparatorProperty().bind(tableViewB.comparatorProperty());

        tableViewB.setItems(sortedDataB);
    }

    /**
     * Pop up para adicionar um jogador na equipa que foi selecionada!
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void AddPlayerPopUpA(ActionEvent actionEvent) throws Exception {
    Jogador player = new Jogador();
    JogadorDAO playerDao = new JogadorDAO();
    ClubeDAO daoClube = new ClubeDAO();
    ArrayList<Clube> clubeAdd;
    String nome = Controller.getInstance().getClubeA();
    clubeAdd = daoClube.findIDClube(nome);

    JTextField tName = new JTextField("Nome");
    JTextField tNumber = new JTextField("Numero");
    JPanel panel = new JPanel(new GridLayout(0, 1));
    panel.add(new JLabel("Nome do jogador:"));
    panel.add(tName);
    panel.add(new JLabel("Numero do jogado"));
    panel.add(tNumber);

    int result = JOptionPane.showConfirmDialog(null, panel, "Inserção de um novo jogador no clube "+nome,
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
    if (result == JOptionPane.OK_OPTION) {
        System.out.println(tName.getText());
        System.out.println(tNumber.getText());
        System.out.println(nome);
        int n = Integer.parseInt(tNumber.getText());
        player.setNome_jogador(tName.getText());
        player.setNumero_jogador(n);
        for (Clube c : clubeAdd){
            player.setId_clube(c.getId_clube());
            System.out.println(c.getId_clube());
        }
    } else {
        System.out.println("Cancelled");
    }
        playerDao.save(player);
}

    /**
     * Pop up para adicionar um jogador na equipa que foi selecionada!
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void AddPlayerPopUpB(ActionEvent actionEvent) throws Exception {
        Jogador player = new Jogador();
        JogadorDAO playerDao = new JogadorDAO();
        ClubeDAO daoClube = new ClubeDAO();
        ArrayList<Clube> clubeAdd;
        String nome = Controller.getInstance().getClubeB();
        clubeAdd = daoClube.findIDClube(nome);

        JTextField tName = new JTextField("Nome");
        JTextField tNumber = new JTextField("Numero");
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Nome do jogador:"));
        panel.add(tName);
        panel.add(new JLabel("Numero do jogado"));
        panel.add(tNumber);

        int result = JOptionPane.showConfirmDialog(null, panel, "Inserção de um novo jogador no clube "+nome,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println(tName.getText());
            System.out.println(tNumber.getText());
            System.out.println(nome);
            int n = Integer.parseInt(tNumber.getText());
            player.setNome_jogador(tName.getText());
            player.setNumero_jogador(n);
            for (Clube c : clubeAdd){
                player.setId_clube(c.getId_clube());
                System.out.println(c.getId_clube());
            }
        } else {
            System.out.println("Cancelled");
        }
        playerDao.save(player);
    }

    /**
     * POP UP para remover um jogador na equipa que foi selecionada
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void RemovePlayerPopUpA(ActionEvent actionEvent) throws Exception {
        Integer index = -1;
            index = tableViewA.getSelectionModel().getSelectedIndex();

            if (index > -1){
                //Recebo o nome do jogador da TableView selecionado
                TableView jogador = (TableView) tableViewA.getSelectionModel().getSelectedItem();
                Controller.getInstance().setNomeJogador(jogador.getNome());
                //Abre um popup para adicionar o evento
                popUpDelete(jogador);
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Aviso");
                alert.setHeaderText("Sem seleção");
                alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
                alert.showAndWait();
            }
        }

    /**
     * POP UP para remover um jogador na equipa que foi selecionada
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    public void RemovePlayerPopUpB(ActionEvent actionEvent) throws Exception {
        Integer index = -1;
        index = tableViewB.getSelectionModel().getSelectedIndex();

        if (index > -1){
            //Recebo o nome do jogador da TableView selecionado
            TableView jogador = (TableView) tableViewB.getSelectionModel().getSelectedItem();
            Controller.getInstance().setNomeJogador(jogador.getNome());
            //Abre um popup para adicionar o evento
            popUpDelete(jogador);
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
            alert.showAndWait();
        }
    }

    /**
     * Pop up para verificar se quer dar delete
     *
     * @param jogador Jogador selecionado pelo utilizador
     * @throws Exception Verificação das exceções
     */
        public void popUpDelete(TableView jogador) throws Exception{
            Jogador player = new Jogador();
            JogadorDAO playerDao = new JogadorDAO();

            JTextArea texto = new JTextArea("Quer mesmo remover o jogador selecionado?");
            JPanel panel = new JPanel(new GridLayout(0, 1));
            panel.add(texto);

            int result = JOptionPane.showConfirmDialog(null, panel, "Eliminação de um novo jogador no clube",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                System.out.println(jogador.getNome());
                System.out.println(jogador.getNum());
                player.setNome_jogador(jogador.getNome());
                player.setNumero_jogador(jogador.getNum());

            } else {
                System.out.println("Cancelled");
            }
            playerDao.delete(player);
        }

    /**
     * Consulta a localização do jogo no TABPANE
     */
    public void consultarLocalizacao() {
        EstadioDAO daoestadio = new EstadioDAO();
        ArrayList<Estadio> estadioadd = new ArrayList<>();
        JogoDAO daojogo = new JogoDAO();
        ArrayList<Jogo> jogoadd = new ArrayList<>();
        ClubeDAO daoclube = new ClubeDAO();
        ArrayList<Clube> clubeadd = new ArrayList<>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        //Seta dados do local do jogo nas labels
        jogoadd = daojogo.findLocalID();
        for (Jogo e : jogoadd){
            estadioID.setText(String.valueOf(e.getId_estadio()));
        }
        Controller.getInstance().setAuxEstadioID(Integer.parseInt(estadioID.getText()));

        clubeadd = daoclube.findLocalClube();
        for (Clube c : clubeadd){
            clube.setText(c.getNome_clube());
        }
        estadioadd = daoestadio.findLocalEstadio();
        for (Estadio e : estadioadd){
            pais.setText(e.getPais());
            cidade.setText(e.getCidade());
            estadio.setText(e.getEstadio());
        }
        atualizarCBLocal();
    }

    /**
     * Atualizar o clube atual para o clube secundario
     */
    private void atualizarCBLocal() {
        obsClubes.clear();
        if (Objects.equals(clube.getText(), Controller.getInstance().getClubeA())){
            obsClubes.add(Controller.getInstance().getClubeB());
        } else {
            obsClubes.add(Controller.getInstance().getClubeA());
        }
        comboBoxEstadio.setItems(obsClubes);
    }


    /**
     * Consultar eventos ao respetivo jogo
     */
    public void consultarEventos() {
        EventoDAO daoevento = new EventoDAO();
        ArrayList<Evento> eventoadd;
        JogadorDAO daojogador = new JogadorDAO();
        ArrayList<Jogador> jogadoradd;
        ClubeDAO daoclube = new ClubeDAO();
        ArrayList<Clube> clubeadd;

        eventoadd = (ArrayList<Evento>) daoevento.findEventos();
            for (Evento e : eventoadd) {
                String nome_clube = "";
                String nome_jogador = "";

                jogadoradd = (ArrayList<Jogador>) daojogador.findNomeJogador(e.getId_jogador());
                for (Jogador j : jogadoradd) {
                    nome_jogador = j.getNome_jogador();
                }
                clubeadd = (ArrayList<Clube>) daoclube.findNomeClubeEvento(e.getId_clube());
                for (Clube c : clubeadd) {
                    nome_clube = c.getNome_clube();
                }

                listaEventos.add("[ADMIN -> EVENTO]: Administrador adicionou um " + e.getDesc_evento() + " ao jogador "
                        + nome_jogador + " do Clube: " + nome_clube + " | MINUTO " + e.getMinuto());
        }

            listViewEventos.setItems(listaEventos);
    }

    /**
     * Define o local do jogo
     */
    public void onDefinirLocal(){
        EventoDAO daoevento = new EventoDAO();
        ArrayList<Evento> eventoadd = new ArrayList<>();
        Evento evento =new Evento();
        JogoDAO daojogo = new JogoDAO();
        ArrayList<Jogo> jogoadd = new ArrayList<>();
        Jogo jogo =new Jogo();
        ClubeDAO daoclube = new ClubeDAO();
        ArrayList<Clube> clubeadd = new ArrayList<>();
        Estadio estadio1 =new Estadio();
        EstadioDAO daoestadio = new EstadioDAO();
        ArrayList<Estadio> estadioadd = new ArrayList<>();


        //Ao clicar no botão OK na Localização do Jogo
        if (comboBoxEstadio.getSelectionModel().getSelectedIndex() == 0) { //clubeA
            eventoadd = (ArrayList<Evento>) daoevento.findOutrosEventos();

            listaEventos.add("[ADMIN -> LOCALIZAÇÃO]: Administrador alterou a Localização do Jogo de:  " + clube.getText() + " para " + comboBoxEstadio.getSelectionModel().getSelectedItem());
            String resultado = "[ADMIN -> LOCALIZAÇÃO]: Administrador alterou a Localização do Jogo de:  " + clube.getText() + " para " + comboBoxEstadio.getSelectionModel().getSelectedItem();
            listViewEventos.setItems(listaEventos);
            clube.setText(comboBoxEstadio.getSelectionModel().getSelectedItem());

            //Adicionar na BASE DE DADOS esse EVENTO
            for (Evento e : eventoadd){
                e.setDesc_evento(resultado);
            }
            daoevento.insertLocalJogo(evento);

            //Atualiza os valores das labels ao clicar no botão OK
            if (clube.getText() == Controller.getInstance().getClubeA()){
                clubeadd = daoclube.findClubeLocalA();
                for (Clube c : clubeadd){
                    estadioID.setText(String.valueOf(c.getId_estadio()));
                    clube.setText(c.getNome_clube());
                }
            } else {
                clubeadd = daoclube.findClubeLocalB();
                for (Clube c : clubeadd){
                    estadioID.setText(String.valueOf(c.getId_estadio()));
                    clube.setText(c.getNome_clube());
                }
            }

            int auxEstadioID = Integer.parseInt(estadioID.getText());
            Controller.getInstance().setAuxEstadioID(auxEstadioID);
            estadioadd = daoestadio.findLocalEstadio();
            for (Estadio e : estadioadd){
                pais.setText(e.getPais());
                cidade.setText(e.getCidade());
                estadio.setText(e.getEstadio());
            }
            //Atualiza o id_estadio da Table Jogo
            daojogo.updateLocalJogo(jogo);
            //Atualiza o comboBox com um clube =! da Label clube
            atualizarCBLocal();

            //Se não selecionar nada do combobox
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione um clube na ComboBox (1.Selecione o clube).");
            alert.showAndWait();
        }
    }
    //EVENTOS

    /**
     * Adicionar eventos à TableViewA
     */
    public void adicionarEventoA(){
        Integer index = -1;
        index = tableViewA.getSelectionModel().getSelectedIndex();

        if (index > -1){
            //Recebo o nome do jogador da TableView selecionado
            TableView jogador = (TableView) tableViewA.getSelectionModel().getSelectedItem();
            Controller.getInstance().setNomeJogador(jogador.getNome());
            //Abre um popup para adicionar o evento
            popUpEvento();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
            alert.showAndWait();
        }
    }

    /**
     * Adicionar eventos à TableViewB
     */
    public void adicionarEventoB(){
        Integer index = -1;
        index = tableViewB.getSelectionModel().getSelectedIndex();

        if (index > -1){
            //Recebo o nome do jogador da TableView selecionado
            TableView jogador = (TableView) tableViewB.getSelectionModel().getSelectedItem();
            Controller.getInstance().setNomeJogador(jogador.getNome());
            //Abre um popup para adicionar o evento
            popUpEvento();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aviso");
            alert.setHeaderText("Sem seleção");
            alert.setContentText("Selecione primeiro um jogador da tabela de jogadores");
            alert.showAndWait();
        }
    }

    /**
     * PopUp chamado ao clicar no botão de adicionar o evento
     */
    public void popUpEvento(){
        String[] items = {"Golo", "Cartão amarelo", "Cartão vermelho"};
        JComboBox<String> combo = new JComboBox<>(items);
        JTextField min = new JTextField("");
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(combo);
        panel.add(new JLabel("Minuto:"));
        panel.add(min);
        int result = JOptionPane.showConfirmDialog(null, panel, "Adicionar evento",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        //Se a pessoa clicar em OK no popup
        if (result == JOptionPane.OK_OPTION) {
            String desc = "";
            int minuto = 0;
            int id_jogo = 0;
            int id_clube = 0;
            int id_jogador = 0;

            Jogador jogador1 =new Jogador();
            JogadorDAO daojogador = new JogadorDAO();
            ArrayList<Jogador> jogadoradd = new ArrayList<>();
            //Busco o ID do jogador pelo nome do jogador recebido
            jogadoradd = (ArrayList<Jogador>) daojogador.findIDJogador();
            for (Jogador j : jogadoradd){
                id_clube = j.getId_clube();
                id_jogador = j.getId_jogador();
                desc = (String) combo.getSelectedItem();
                minuto = Integer.parseInt(min.getText());
            }
            //Busca o ID do jogo com a desc_jogo
            JogoDAO daojogo = new JogoDAO();
            ArrayList<Jogo> jogoadd = new ArrayList<>();
            Jogo jogo =new Jogo();
            jogoadd = daojogo.findIDJogo();
            for (Jogo j : jogoadd){
                id_jogo = j.getId_jogo();
            }

            //Insere o evento na base de dados
            Evento evento1 =new Evento();
            EventoDAO daoevento = new EventoDAO();
            evento1.setDesc_evento(desc);
            evento1.setMinuto(minuto);
            evento1.setId_jogo(id_jogo);
            evento1.setId_clube(id_clube);
            evento1.setId_jogador(id_jogador);
            daoevento.insertEvento(evento1);
            listaEventos.add("[ADMIN -> EVENTO]: Administrador adicionou um "+desc+" ao jogador "+Controller.getInstance().getNomeJogador()
                    +" do Clube "+Controller.getInstance().getClubeA()+" | MINUTO "+minuto);
            listViewEventos.setItems(listaEventos);
        } else {
            System.out.println("Cancelado");
        }
    }

    /**
     * butão para aparecer as estatisticas do clube A
     *
     * @param event Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onbuttonClubeA(ActionEvent event)throws Exception{
        Stage window = (Stage) buttonClubeA.getScene().getWindow();
        window.close();
        SwitchMenus.open("AdminClube", "SUPERLIGA >> Admin >> Estatisticas");

    }

    /**
     * Butão para aparecer as estatisticas do clube A
     *
     * @param event Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onbuttonClubeB(ActionEvent event)throws Exception{
        Stage window = (Stage) buttonClubeB.getScene().getWindow();
        window.close();
        SwitchMenus.open("AdminClube", "SUPERLIGA >> Admin >> Estatisticas");

    }
    
    /**
     * Ao clicar no botão, volta para o menu Admin > Jornada
     *
     * @param actionEvent Ação do evento
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("AdminJornada", "SUPERLIGA >> Admin >> Jornada");
    }
}
