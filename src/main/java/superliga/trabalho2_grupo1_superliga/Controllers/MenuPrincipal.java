package superliga.trabalho2_grupo1_superliga.Controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class MenuPrincipal {
    @FXML
    protected Button button;
    @FXML
    protected Button button2;
    @FXML
    protected Button button3;
    @FXML
    protected Button button4;

    /**
     * Inicia a scene com o conteúdo desta classe
     */
    public void initialize(){
        if (Controller.getInstance().getEsconderBotoes().equals("2")){
            button.setDisable(true);
            button4.setDisable(true);
        } else if (Controller.getInstance().getEsconderBotoes().equals("3")) {
            button.setDisable(true);
            button2.setDisable(true);
            button4.setDisable(true);
        }
    }

    /**
     * Botão para mudar para o menu principal
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToMenuAdm() throws Exception {
        //Remove o stage criado com o erro de Null
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
        SwitchMenus.open("AdminJornada", "SUPERLIGA >> Admin >> Jornadas");
    }

    /**
     * Botão para mudar para o menu operador
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToMenuOp() throws Exception {
        //Remove o stage criado com o erro de Null
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
        SwitchMenus.open("OperadorJornada", "SUPERLIGA >> Operador >> Jornadas");
    }

    /**
     * Botão para mudar para o menu Milionario
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToMenuMili() throws Exception {
        //Remove o stage criado com o erro de Null
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
        SwitchMenus.open("MenuMilionario", "SUPERLIGA >> Milionário >> Estatisticas");
    }

    /**
     * Botão para mudar para o menu de cadastro
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToRegister() throws Exception {
        //Remove o stage criado com o erro de Null
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
        SwitchMenus.open("Register", "SUPERLIGA >> Registro");
    }

    /**
     * Botão para mudar para o menu de login
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    protected void switchToLogin() throws Exception {
        //Remove o stage criado com o erro de Null
        Stage window = (Stage) button.getScene().getWindow();
        window.close();
        SwitchMenus.open("Login", "SUPERLIGA >> Login");
    }

    /**
     * Botão para mudar para o menu principal
     *
     * @throws Exception Verificação das exceções
     */
    @FXML
    public void onVoltar(ActionEvent actionEvent) throws Exception {
        SwitchMenus.open("MenuPrincipal", "SUPERLIGA | Menu Principal");
    }
}