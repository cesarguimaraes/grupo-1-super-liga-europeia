package Classes;

/**
 *
 * @author Rodrigo Morais (1210536)
 */
public class Classificacao {
    /**
     * nome do jogador.
     */
    private String nome_clubes;
    /**
     * numero do jogador.
     */
    private int pontos;


    /**
     * Constrói uma intância de Classificaçao
     *
     * @param nome_clubes nome do clube
     * @param pontos numero de pontos da equipa
     */
    public Classificacao(String nome_clubes, int pontos){
        this.nome_clubes=nome_clubes;
        this.pontos=pontos;
    }

    /**
     * Constrói uma intância da Classificaçao vazia
     */
    public Classificacao(){
        this.nome_clubes="sem clube";
        this.pontos=0;
    }

    /**
     * @return o nome do nome clube
     */
    public String getNome_clube() {
        return nome_clubes;
    }

    /**
     * @return os pontos que a equipa teve
     */
    public int getPontos() {
        return pontos;
    }

    //-------------------------------------------------------------------------------

    /**
     * @param nome_clubes modifica o nome do clube
     */
    public void setNome_clubes(String nome_clubes) {
        this.nome_clubes = nome_clubes;
    }

    /**
     * Modifica o nome_clube
     * @param pontos Modifica os pontos que o clube tem
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }


}