package Classes;

/**
 *
 * @author Rodrigo Morais (1210536)
 */
public class Clube {
    private int id_clube;
    private String nome_clube;

    private int pontos;

    private int id_estadio;




    /**
     * Constrói de um jogador
     *
     * @param nome_clube nome do clube
     * @param estadio estadio que o clube usa
     * @param cidade cidade que o clube esta inserida
     * @param pais pais de origem do clube
     */
    public Clube(int id_clube, String nome_clube, int pontos, int id_estadio){
        this.id_clube=id_clube;
        this.nome_clube=nome_clube;
        this.pontos=pontos;
        this.id_estadio=id_estadio;
    }

    /**
     * Constrói uma intância do Jogador vazia
     */
    public Clube(){
        this.nome_clube="sem nome";
        this.pontos=0;
    }

    /**
     * Recebe o id do clube
     * @return id_clube
     */
    public int getId_clube() {
        return id_clube;
    }

    /**
     * Recebe o nome do clube
     *
     * @return nome_clube
     */
    public String getNome_clube() {
        return nome_clube;
    }

    public int getPontos() {
        return pontos;
    }

    /**
     * Recebe o id do estádio do clube
     * @return id_estadio
     */
    public int getId_estadio() {
        return id_estadio;
    }
    //-------------------------------------------------------------------------------


    public void setId_clube(int id_clube) {
        this.id_clube = id_clube;
    }

    /**
     * Modifica o nome_clube
     * @param nome_clube nome do clube
     */
    public void setNome_clube(String nome_clube) {
        this.nome_clube = nome_clube;
    }

    /**
     * Seta pontos para um clube
     * @param pontos
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    /**
     * Seta o id_estadio
     * @param id_estadio
     */
    public void setId_estadio(int id_estadio) {
        this.id_estadio = id_estadio;
    }


}