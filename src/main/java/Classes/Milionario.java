package Classes;

import Connect.ConnectionBd;

import java.sql.Connection;

/**
 * Classe pública em que tem todos os métodos do Milionario
 */
public class Milionario extends Clube {
    private final Connection con = ConnectionBd.establishConnection();

    private String nome_clube;
    private String nome_jogador;

    private int qtd;

    /**
     * Construtor completo
     * @param nome_clube
     * @param qtd
     */
    public Milionario(String nome_clube, String nome_jogador, int qtd) {
        this.nome_clube = nome_clube;
        this.nome_jogador = nome_jogador;
        this.qtd = qtd;
    }

    public Milionario() {

    }

    public String getNome_clubeMil() {
        return nome_clube;
    }

    public void setNome_clubeMil(String nome_clube) {
        this.nome_clube = nome_clube;
    }

    public String getNome_jogadorMil() {
        return nome_jogador;
    }

    public void setNome_jogadorMil(String nome_jogador) {
        this.nome_jogador = nome_jogador;
    }
    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }
}
