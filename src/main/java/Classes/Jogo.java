package Classes;

/**
 * @author César Guimarães (1210522)
 * Classe Principal Eventos_Jogo
 */
public class Jogo {
    private int id_jogo;

    private String desc_jogo;
    private int id_clubeA;
    private String nome_clubeA;
    private int id_clubeB;
    private String nome_clubeB;

    private int id_estadio;

    /**
     * Construtor dos jogos
     */
    public Jogo(int id_jogo, String num_jogo, int id_clubeA,String nome_clubeA, int id_clubeB,String nome_clubeB ,int id_estadio){
        this.id_jogo=id_jogo;
        this.desc_jogo = num_jogo;
        this.id_clubeA=id_clubeA;
        this.nome_clubeA=nome_clubeA;
        this.id_clubeB=id_clubeB;
        this.nome_clubeB=nome_clubeB;
        this.id_estadio=id_estadio;
    }
    /**
     * Construtor vasio dos jogos
     */
    public Jogo() {
        this.id_jogo=0;
        this.desc_jogo = "";
        this.id_clubeA=0;
        this.nome_clubeA="";
        this.id_clubeB=0;
        this.nome_clubeB="";
        this.id_estadio=0;
    }

    /**
     * Receber o id dos jogos
     * @return id_jogo
     */
    public int getId_jogo(){return id_jogo;}

    /**
     * Receber a descriçao do jogo
     * @return desc_jogo
     */
    public String getDesc_jogo(){return desc_jogo;}

    /**
     * Receber o id da equipa A
     * @return id_clubeA
     */
    public int getId_clubeA(){
        return id_clubeA;
    }

    /**
     * Receber o nome do clube A
     * @return nome_clubeA
     */
    public String getNome_clubeA(){return nome_clubeA;}
    /**
     * Receber o id da equipa B
     * @return id_clubeB
     */
    public int getId_clubeB(){
        return id_clubeB;
    }
    /**
     * Receber o nome da equipa B
     * @return nome_clubeB
     */
    public String getNome_clubeB(){return nome_clubeB;}

    /**
     * Receber o id do estadio
     * @return id_estadio
     */
    public int getId_estadio(){return id_estadio;}

    /**
     * Set o id do jogo
     * @param id_jogo
     */
    public void setId_jogo(int id_jogo){
        this.id_jogo = id_jogo;
    }

    /**
     * Set da descriçao do jogo
     * @param desc_jogo
     */
    public void setDesc_jogo(String desc_jogo){
        this.desc_jogo = desc_jogo;
    }

    /**
     * Set o id do clube da equipa A
     * @param id_clubeA
     */
    public void setId_clubeA(int id_clubeA){
        this.id_clubeA = id_clubeA;
    }

    /**
     * Set o nome do clube A
     * @param nome_clubeA
     */
    public void setNome_clubeA(String  nome_clubeA){
        this.nome_clubeA = nome_clubeA;
    }

    /**
     * Set id do clube B
     * @param id_clubeB
     */
    public void setId_clubeB(int id_clubeB){
        this.id_clubeB = id_clubeB;
    }
    public void setNome_clubeB(String nome_clubeB){
        this.nome_clubeB = nome_clubeB;
    }

    /**
     * Set o id do estadio do em que o jogo ocorre
     * @param id_estadio
     */
    public void setId_estadio(int id_estadio){
        this.id_estadio = id_estadio;
    }


}
