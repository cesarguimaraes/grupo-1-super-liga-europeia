package Classes;

/**
 *
 * @author RodrigoMorais(121053)
 */
public class Jogador {
    /**
     * nome do jogador.
     */
    private String nome_jogador;
    /**
     * nome do clube do jogador.
     */
    private String nome_clube;

    private int id_clube;

    /**
     * numero do jogador.
     */
    private int numero_jogador;

    private int id_jogador;

    public int getId_clube() {
        return id_clube;
    }

    public void setId_clube(int id_clube) {
        this.id_clube = id_clube;
    }
    public int getId_jogador() {
        return id_jogador;
    }

    public void setId_jogador(int id_jogador) {
        this.id_jogador = id_jogador;
    }

    /**
     * Constrói de um jogador
     *
     * @param nome_jogador nome do jogador
     * @param nome_clube nome do clube do jogador
     * @param numero_jogador numero do jogador
     */
    public Jogador(String nome_jogador, String nome_clube, int numero_jogador){
        this.nome_jogador=nome_jogador;
        this.nome_clube=nome_clube;
        this.numero_jogador=numero_jogador;
    }

    /**
     * Constrói uma intância do Jogador vazia
     */
    public Jogador(){
        this.nome_jogador="sem nome";
        this.nome_clube="sem clube";
        this.numero_jogador=0;
    }

    /**
     * @return o nome do jogador
     */
    public String getNome_jogador() {
        return nome_jogador;
    }

    /**
     * @return o nome do clube em que o jogador estar
     */
    public String getNome_clube() {
        return nome_clube;
    }

    /**
     * @return o numero do jogador
     */
    public int getNumero_jogador() {
        return numero_jogador;
    }

    //-------------------------------------------------------------------------------

    /**
     * Modifica o nome do jogador
     * @param nome_jogador nome do jogador
     */
    public void setNome_jogador(String nome_jogador) {
        this.nome_jogador = nome_jogador;
    }

    /**
     * Modifica o nome_clube
     * @param nome_clube nome do clube
     */
    public void setNome_clube(String nome_clube) {
        this.nome_clube = nome_clube;
    }

    /**
     * Modifica numero do jogador
     * @param numero_jogador numero do jogador
     */
    public void setNumero_jogador(int numero_jogador) {
        this.numero_jogador = numero_jogador;
    }
}
