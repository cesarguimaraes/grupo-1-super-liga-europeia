package Classes;

/**
 *
 * @author Tiago Ferraz
 */
public class Estatistica {

    private String marcador_geral, marcador_equipa, equipa_mais_derrotas;

    //Gets e sets
    /**
     * Devolve o marcador geral do jogo
     *
     * @return marcador geral do jogo
     */
    public String getMarcador_geral() {
        return marcador_geral;
    }

    /**
     * Modifica o marcador geral desse mesmo jogo
     *
     * @param marcador_geral marcador geral do jogo
     */
    public void setMarcador_geral(String marcador_geral) {
        this.marcador_geral = marcador_geral;
    }

    /**
     * Devolve o marcador da equipa selecionada desse mesmo jogo
     *
     * @return marcador da equipa
     */
    public String getMarcador_equipa() {
        return marcador_equipa;
    }

    /**
     * Modifica o marcador da equipa selecionada desse mesmo jogo
     *
     * @param marcador_equipa marcador da quipa
     */
    public void setMarcador_equipa(String marcador_equipa) {
        this.marcador_equipa = marcador_equipa;
    }

    /**
     * Devolve a equipa com mais derrotas na classificação
     *
     * @return Equipa com mais derrotas
     */
    public String getEquipa_mais_derrotas() {
        return equipa_mais_derrotas;
    }

    /**
     * Modifica a equipa com mais derrotas na classificação
     *
     * @param equipa_mais_derrotas equipa com mais derrotas
     */
    public void setEquipa_mais_derrotas(String equipa_mais_derrotas) {
        this.equipa_mais_derrotas = equipa_mais_derrotas;
    }

    /**
     * Escreve o nome do jogo, o nome do estadio, cidade e o pais do mesmo
     *
     * @return nome do jogo, o nome do estadio, cidade e o pais
     */
    @Override
    public String toString() {
        return "Estatistica{" + "marcador_geral=" + marcador_geral + ", marcador_equipa=" + marcador_equipa + ", equipa_mais_derrotas=" + equipa_mais_derrotas + '}';
    }

}
