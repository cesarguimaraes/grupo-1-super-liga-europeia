package Classes;
/**
 *
 * @author Rodrigo Morais
 */
public class Tipos_evento {
    String descricao;

    /**
     * Construtor para adicionar/alterar os valores da localização do jogo
     *
     * @param descricao nome do jogo
     */
    public Tipos_evento(String descricao) {
        this.descricao = descricao;
    }

    //gets e sets
    /**
     * Devolve a descricao do evento
     *
     * @return descricao do evento
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Modifica a descricao do evento
     *
     * @param descricao decicao do evento
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    @Override
    public String toString() {
        return "Evento: {" + "descicao=" + descricao ;}
}