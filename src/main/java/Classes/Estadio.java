/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Classes;

/**
 *
 * @author Tiago Ferraz
 */
public class Estadio {

    private int id_estadio;
    private String estadio, cidade, pais;

    /**
     * Construtor completo
     * @param id_estadio
     * @param estadio
     * @param cidade
     * @param pais
     */
    public Estadio(int id_estadio, String estadio, String cidade, String pais) {
        this.id_estadio = id_estadio;
        this.estadio = estadio;
        this.cidade = cidade;
        this.pais = pais;
    }

    /**
     * Construtor padrão
     */
    public Estadio() {

    }

    public int getId_estadio() {
        return id_estadio;
    }

    public void setId_estadio(int id_estadio) {
        this.id_estadio = id_estadio;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Escreve o nome do jogo, o nome do estadio, cidade e o pais do mesmo
     *
     * @return nome do jogo, o nome do estadio, cidade e o pais
     */
    @Override
    public String toString() {
        return "Estadio{" + "nome=" + ", estadio=" + estadio + ", cidade=" + cidade + ", pais=" + pais + '}';
    }

}
