package Classes.dao;

import Classes.Clube;
import Classes.Jornada;
import Connect.ConnectionBd;
import javafx.event.ActionEvent;
import superliga.trabalho2_grupo1_superliga.Controllers.AdminJogos;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClubeDAO {
    /**
     * @param con cria a variavel para realizar a conexão a base de dados
     */
    private Connection con;

    /**
     * liga a base de dados
     */
    public ClubeDAO() {
        con = ConnectionBd.establishConnection();
    }
    /**
     * Metodo para inserir no clube
     * @param clube a variavel que guardara a informaçao do tipo clube
     */
    public boolean save(Clube clube) {
        String sql = "INSERT INTO Clube(nome_clube, estadio, cidade, pais) VALUES (?,?,?,?)";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, clube.getId_clube());
            stmt.setString(2, clube.getNome_clube());
            stmt.setInt(3, clube.getPontos());
            stmt.setInt(4, clube.getId_estadio());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para pesquisar os nomes dos clubes
     */
    public ArrayList<Clube> findClube() {
        String sql = "SELECT nome_clube FROM Clube";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }
    /**
     * Metodo para procurar o nome dos clubes a partir do id do estatio
     */
    public ArrayList<Clube> findLocalClube() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT nome_clube FROM Clube WHERE id_estadio='"+Controller.getInstance().getAuxEstadioID()+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para procurar o nome dos clubes da equipa que joga em casa a partir do id do estatio
     */
    public ArrayList<Clube> findClubeLocalA() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT id_estadio, nome_clube FROM Clube WHERE nome_clube = '" + Controller.getInstance().getClubeA() + "'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setId_estadio(rs.getInt("id_estadio"));
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }
    /**
     * Metodo para procurar o nome dos clubes da equipa que esta a jogar fora a partir do id do estatio
     */
    public ArrayList<Clube> findClubeLocalB() {
        ConnectionBd.establishConnection();
        String sql = "SELECT id_estadio, nome_clube FROM Clube WHERE nome_clube = '" + Controller.getInstance().getClubeB() + "'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setId_estadio(rs.getInt("id_estadio"));
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para procurar o id do clube a partir do nome
     */
    public ArrayList<Clube> findIDClube(String nomeClub) {
        String sql = "SELECT id_clube FROM Clube WHERE nome_clube='" +nomeClub+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setId_clube(rs.getInt("id_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para procurar o nome do clube a partir do id
     */
    public ArrayList<Clube> findNomeClube() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT nome_clube FROM Clube WHERE id_clube=" + Controller.getInstance().getId_clube();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setNome_clube(club.getNome_clube());
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para procurar o nome do clube a partir do id
     */
    public ArrayList<Clube> findNomeClubeEvento(int id_clube) {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT nome_clube FROM Clube WHERE id_clube=" + id_clube;

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para atualizar o clube
     * @param clube variavel para dar update no clube
     */
    public boolean update(Clube clube) {
        String sql = "UPDATE Clube SET nome_clube = ? WHERE id_clube = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, clube.getNome_clube());
            stmt.setInt(2, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para apagar um clube
     * @param clube variavel tipo clube que ira guardar a informação
     */
    public boolean delete(Clube clube) {
        String sql = "DELETE FROM clube WHERE nome_clube = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,clube.getNome_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /*
    public static void clubInsert() {

        Clube club = new Clube("Braga", "Estádio Emirates", "Londres", "Inglaterra");
        ClubeDAO dao = new ClubeDAO();

        if (dao.save(club)) {
            System.out.println("O clube foi adicionado com sucesso!");
        } else {
            System.out.println("Erro ao guardar o clube na base de dados!");
        }
    }
    public static void clubUpdate() {
        Clube club = new Clube("Sporting", "Estádio Emirates", "Londres", "Inglaterra");
        club.setId_clube(2);
        ClubeDAO dao = new ClubeDAO();

        if (dao.update(club)) {
            System.out.println("O clube foi atualizado com sucesso!");
        } else {
            System.out.println("Erro ao atualizar!");
        }
    }
    public static void clubDelete() {
        Clube club = new Clube();
        club.setNome_clube("Braga");

        ClubeDAO dao = new ClubeDAO();

        if (dao.delete(club)) {
            System.out.println("O clube foi eliminado com sucesso!");
        } else {
            System.out.println("Erro ao eliminar o clube");
        }
    }

    public static void clubList() {
        ClubeDAO dao = new ClubeDAO();

        for (Clube c : dao.findClube()) {

            System.out.println("Nome do Clube: " + c.getNome_clube() + " Estadio: " + c.getEstadio() + " Cidade: " + c.getCidade() + " Pais: " + c.getPais());
        }
    }
     */
}
