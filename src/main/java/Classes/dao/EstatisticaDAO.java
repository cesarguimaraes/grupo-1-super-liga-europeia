package Classes.dao;

import Classes.Clube;
import Classes.Estatistica;
import Connect.ConnectionBd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstatisticaDAO {
    private final Connection con;

    public EstatisticaDAO() {
        con = ConnectionBd.establishConnection();
    }

   /* public boolean save(Estatistica estatistica) {
        String sql = "INSERT INTO Clube(marcador_geral, marcador_equipas, equipas_derrotas, pais) VALUES (?,?,?,?)";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, estatistica.getMarcador_geral());
            stmt.setString(2, estatistica.getMarcador_equipa());
            stmt.setString(3, estatistica.getEquipa_mais_derrotas());
            stmt.setString(4, estatistica.getPais());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    public List<Clube> findAll() {

        String sql = "SELECT * FROM clube";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Clube> clubes = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Clube clube = new Clube();
                clube.setNome_clube(rs.getString("nome_clube"));
                clube.setEstadio(rs.getString("estadio"));
                clube.setCidade(rs.getString("cidade"));
                clube.setPais(rs.getString("pais"));
                clubes.add(clube);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return clubes;
    }


    public boolean update(Clube clube) {
        String sql = "UPDATE Clube SET nome_clube = ? WHERE id_clube = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, clube.getNome_clube());
            stmt.setInt(2, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }


    public boolean delete(Clube clube) {
        String sql = "DELETE FROM clube WHERE id_clube = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }*/
}
