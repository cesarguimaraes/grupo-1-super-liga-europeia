package Classes.dao;

import Classes.Clube;
import Classes.Jornada;
import Connect.ConnectionBd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JornadaDAO {
    private Connection con = null;

    /**
     * Coneçao a base de dados
     * @var con
     */
    public JornadaDAO() {
        con = ConnectionBd.establishConnection();
    }

    /**
     * Inserir jornadas na base de dados
     * @sql variavel tipo String que guarda do inserte a ser usado
     * @stmt variavel que insere na base de dados
     */
    public boolean insertJornada(Jornada jornada){
        String sql = "INSERT INTO Jornada (desc_jornada) VALUES (?)";
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,jornada.getDesc_jornada());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jornada " +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }

    /**
     * pesquisar jornadas na base de dados
     * @sql variavel tipo String que ira realizar o select na base de dados
     * @stmt variavel que insere na base de dados
     * @jorn variavel que guarda a informaçao dos campos do select
     */
    public ArrayList<Jornada> findJornada(){
        String sql = "SELECT  desc_jornada FROM Jornada ORDER BY desc_jornada";

        PreparedStatement stmt = null;
        ResultSet rs = null;

                ArrayList<Jornada> listdejornadas = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Jornada jorn = new Jornada();
                jorn.setDesc_jornada(rs.getString("desc_jornada"));
                listdejornadas.add(jorn);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jornada" +e.getMessage());
        }finally {
            ConnectionBd.closeConnection(con,stmt,rs);
        }
        return listdejornadas;
    }

    /**
     * update jornadas na base de dados
     * @sql variavel tipo String que guarda do update a ser usado
     * @stmt variavel que da o update na base de dados
     */
   public boolean update(Clube clube){
        String sql = "UPDATE Clube SET nome_clube = ? WHERE id_clube = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,clube.getNome_clube());
            stmt.setInt(2, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }

    /**
     * Apagar jornadas na base de dados
     * @sql variavel tipo String que guarda do delete a ser usado
     * @stmt variavel que apaga na base de dados
     */
    public boolean deleteJornada(Jornada jornada){
        String sql = "DELETE FROM jornada WHERE desc_jornada = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, jornada.getDesc_jornada());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados da Jornada" +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }
}
