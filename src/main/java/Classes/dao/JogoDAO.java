package Classes.dao;

import Classes.Jogo;
import Classes.Jornada;
import Connect.ConnectionBd;
import superliga.trabalho2_grupo1_superliga.Controllers.AdminJogos;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class JogoDAO {
    /**
     * @param con cria a variavel para realizar a conexão a base de dados
     */
    private Connection con = null;
    /**
     * liga a base de dados
     */
    public JogoDAO() {
        con = ConnectionBd.establishConnection();
    }

    /**
     * Inserir jogos na base de dados
     * @sql variavel tipo String que guarda do inserte a ser usado
     * @stmt variavel que insere na base de dados
     */
    public boolean insertJogo(Jogo jogo) {
        String sql = "INSERT INTO Jogo (desc_jogo, id_clubeA, id_clubeB, id_estadio, nome_clubeA, nome_clubeB)" +
                " VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, jogo.getDesc_jogo());
            stmt.setInt(2, jogo.getId_clubeA());
            stmt.setInt(3, jogo.getId_clubeB());
            stmt.setInt(4, jogo.getId_estadio());
            stmt.setString(5, jogo.getNome_clubeA());
            stmt.setString(6, jogo.getNome_clubeB());

            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("insertJogo | Erro na base dados do JogoDAO " + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo usado para obter o id do estadio a partir da descriçao
     */
    public ArrayList<Jogo> findLocalID(){
        String sql = "SELECT id_estadio FROM Jogo WHERE desc_jogo='"
                +Controller.getInstance().getJogoAtual().substring
                (0, Controller.getInstance().getJogoAtual().indexOf(">")-1).trim()+"'";


        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Jogo> listaLocalID = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()){
                Jogo jog = new Jogo();
                jog.setId_estadio(rs.getInt("id_estadio"));
                listaLocalID.add(jog);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogo" +e.getMessage());
        }finally {
            ConnectionBd.closeConnection(con,stmt,rs);
        }
        return listaLocalID;
    }

    /**
     * Select dos jogos na base de dados
     * @sql variavel tipo String que guarda do inserte a ser usado
     * @stmt variavel que pesquisa na base de dados
     */
    public ArrayList<Jogo> findJogo(/*int idjornada*/){
        int jogo;
        String testeinner = Arrays.toString(Controller.getInstance().getJornadaAtual().
                    split("[a-z]"))
                .replace(",","")
                .replace("J","")
                .replace("[","")
                .replace("]","").trim();
        jogo= Integer.parseInt(testeinner);

        String sql = "select desc_jogo, nome_clubeA, nome_clubeB " +
                "from Jogo inner join JornadaJogo " +
                "on Jogo.id_jogo=JornadaJogo.id_jogo and JornadaJogo.id_jornada= "+jogo;


        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Jogo> listdejogos = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()){
                Jogo jog = new Jogo();
                jog.setDesc_jogo(rs.getString("desc_jogo").trim());
                jog.setNome_clubeA(rs.getString("nome_clubeA").trim());
                jog.setNome_clubeB(rs.getString("nome_clubeB").trim());
                listdejogos.add(jog);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogo" +e.getMessage());
        }finally {
            ConnectionBd.closeConnection(con,stmt,rs);
        }
        return listdejogos;
    }

    /**
     * Metodo usado para obter o id_jogo a partir da descriçao
     */
    public ArrayList<Jogo> findIDJogo(){

        String sql = "SELECT id_jogo FROM Jogo WHERE desc_jogo='"+Controller.getInstance().getJogoAtual()
                .substring(0, Controller.getInstance().getJogoAtual().indexOf(">")-1).trim()+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Jogo> listdejogos = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()){
                Jogo jog = new Jogo();
                jog.setId_jogo(rs.getInt("id_jogo"));
                listdejogos.add(jog);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogo" +e.getMessage());
        }finally {
            ConnectionBd.closeConnection(con,stmt,rs);
        }
        return listdejogos;
    }

    /**
     * Atualizar o estádio para o jogo de 2 clubes
     */
    public boolean updateLocalJogo(Jogo jogo){
        String sql = "UPDATE Jogo SET id_estadio = ? WHERE desc_jogo = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1,Controller.getInstance().getAuxEstadioID());
            stmt.setString(2,Controller.getInstance().getJogoAtual()
                    .substring(0, Controller.getInstance().getJogoAtual().indexOf(">")-1).trim());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("updateEstadio | Erro na base dados do JogoDAO " +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }




    /**
     * Delete dos jogos na base de dados
     * @sql variavel tipo String que guarda do delete a ser usado
     * @stmt variavel que apaga da base de dados
     */
    public boolean deleteJogo(Jogo jogo){
        String sql = "DELETE FROM Jogo WHERE id_jogo = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, jogo.getId_jogo());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }
}
