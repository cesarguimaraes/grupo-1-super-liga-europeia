package Classes.dao;

import Classes.Clube;
import Classes.Estadio;
import Classes.Milionario;
import Connect.ConnectionBd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MilionarioDAO {
    private Connection con;

    public MilionarioDAO() {
        con = ConnectionBd.establishConnection();
    }

    public ArrayList<Milionario> findMarcadorGeral() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, COUNT(Evento.desc_evento) as 'golos'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Golo'\n" +
                "GROUP BY Clube.nome_clube ORDER BY golos desc\n";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setQtd(rs.getInt("golos"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }

    public ArrayList<Milionario> findAmareloGeral() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, COUNT(Evento.desc_evento) as 'cartao_amarelo'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Cartão amarelo'\n" +
                "GROUP BY Clube.nome_clube ORDER BY cartao_amarelo desc\n";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setQtd(rs.getInt("cartao_amarelo"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }


    public ArrayList<Milionario> findVermelhoGeral() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, COUNT(Evento.desc_evento) as 'cartao_vermelho'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Cartão vermelho'\n" +
                "GROUP BY Clube.nome_clube ORDER BY cartao_vermelho desc\n";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setQtd(rs.getInt("cartao_vermelho"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }

    public ArrayList<Milionario> findMarcadorEquipa() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, Jogador.nome_jogador, COUNT(Evento.desc_evento) as 'golos'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Golo'\n" +
                "INNER JOIN Jogador\n" +
                "ON Evento.id_jogador = Jogador.id_jogador\n" +
                "GROUP BY Clube.nome_clube, Jogador.nome_jogador ORDER BY Clube.nome_clube desc";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setNome_jogadorMil(rs.getString("nome_jogador"));
                mil1.setQtd(rs.getInt("golos"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }

    public ArrayList<Milionario> findAmareloEquipa() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, Jogador.nome_jogador, COUNT(Evento.desc_evento) as 'cartao_amarelo'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Cartão amarelo'\n" +
                "INNER JOIN Jogador\n" +
                "ON Evento.id_jogador = Jogador.id_jogador\n" +
                "GROUP BY Clube.nome_clube, Jogador.nome_jogador ORDER BY Clube.nome_clube desc";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setNome_jogadorMil(rs.getString("nome_jogador"));
                mil1.setQtd(rs.getInt("cartao_amarelo"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }

    public ArrayList<Milionario> findVermelhoEquipa() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT Clube.nome_clube, Jogador.nome_jogador, COUNT(Evento.desc_evento) as 'cartao_vermelho'\n" +
                "FROM Evento\n" +
                "INNER JOIN Clube\n" +
                "ON Evento.id_clube = Clube.id_clube and Evento.desc_evento='Cartão vermelho'\n" +
                "INNER JOIN Jogador\n" +
                "ON Evento.id_jogador = Jogador.id_jogador\n" +
                "GROUP BY Clube.nome_clube, Jogador.nome_jogador ORDER BY Clube.nome_clube desc";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Milionario> listaMarcadorGeral1 = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Milionario mil1 = new Milionario();
                mil1.setNome_clubeMil(rs.getString("nome_clube"));
                mil1.setNome_jogadorMil(rs.getString("nome_jogador"));
                mil1.setQtd(rs.getInt("cartao_vermelho"));
                listaMarcadorGeral1.add(mil1);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do MilionarioDAO " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaMarcadorGeral1;
    }

}
