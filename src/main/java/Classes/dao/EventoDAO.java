package Classes.dao;

import Classes.Evento;
import Classes.Jogador;
import Classes.Jogo;
import Connect.ConnectionBd;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventoDAO {
    /**
     * @param con cria a variavel para realizar a conexão a base de dados
     */
    private Connection con;

    /**
     * ligaçao a base de dados
     */
    public EventoDAO(){
        con = ConnectionBd.establishConnection();
    }

    /**
     * Metodo para pesquisar os eventos
     */
    public List<Evento> findEventos() {
        String sql = "SELECT Evento.desc_evento, Evento.minuto, Evento.id_jogo, Evento.id_clube, Evento.id_jogador\n" +
                "FROM Evento\n" +
                "INNER JOIN Jogo\n" +
                "ON Jogo.id_jogo = Evento.id_jogo\n" +
                "WHERE Jogo.desc_jogo='"+Controller.getInstance().getJogoAtual()
                .substring(0, Controller.getInstance().getJogoAtual().indexOf(">")-1)+"'".trim() + "ORDER BY minuto desc";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Evento> eventos = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Evento evento = new Evento();
                evento.setDesc_evento(rs.getString("desc_evento"));
                evento.setMinuto(rs.getInt("minuto"));
                evento.setId_jogo(rs.getInt("id_jogo"));
                evento.setId_clube(rs.getInt("id_clube"));
                evento.setId_jogador(rs.getInt("id_jogador"));
                eventos.add(evento);
            }

        } catch (SQLException e) {
            System.err.println("findEventos | Erro na base dados do Evento " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return eventos;
    }

    /**
     * Metodo para pesquisar os eventos a partir do id do jogo
     */
    public List<Evento> findOutrosEventos() {
        String sql = "SELECT desc_evento FROM Evento WHERE id_jogo='"+Controller.getInstance().getJogoAtual()
                .substring(0, Controller.getInstance().getJogoAtual().indexOf(">")-1)
                .replace("jogo", "").trim();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Evento> eventos = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Evento evento = new Evento();
                evento.setDesc_evento(rs.getString("desc_evento"));
                eventos.add(evento);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return eventos;
    }

    /**
     * Metodo para pesquisar os eventos
     * @param evento variavel tipo Evento
     */
    public boolean update(Evento evento) {
        String sql = "UPDATE Evento SET id_estadio = ? WHERE desc_jogo = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);

            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogador" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }
    /**
     * Metodo para apagar um jogador
     * @param player variavel tipo Jogador
     */
    public boolean delete(Jogador player) {
        String sql = "DELETE FROM Jogador WHERE nome_jogador = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,player.getNome_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogador" + e.getMessage());
            return false;
        } finally {
        }
    }

    /**
     * Metodo para inserir  um evento
     * @param evento variavel tipo Evento
     */
    public boolean insertEvento(Evento evento){
        String sql = "INSERT INTO Evento (desc_evento, minuto, id_jogo, id_clube, id_jogador) " +
                "VALUES (?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, evento.getDesc_evento());
            stmt.setInt(2, evento.getMinuto());
            stmt.setInt(3, evento.getId_jogo());
            stmt.setInt(4, evento.getId_clube());
            stmt.setInt(5, evento.getId_jogador());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("insertEvento | Erro na base dados do Evento " +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }

    /**
     * Metodo para inserir  um evento
     * @param evento variavel tipo Evento
     */
    public boolean insertLocalJogo(Evento evento){
        String sql = "INSERT INTO Evento (desc_evento, id_jogo) VALUES (?, ?)";

        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, evento.getDesc_evento());
            stmt.setInt(2, Integer.parseInt(Controller.getInstance().getJogoAtual()
                    .substring(0, Controller.getInstance().getJogoAtual().indexOf(">")-1)
                    .replace("jogo", "").trim()));
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("insertLocalJogo | Erro na base dados do Evento " +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }

    public static void PlayerUpdate() {
        Jogador player = new Jogador("Marega", "Sporting", 25);
        player.setNumero_jogador(1);
        JogadorDAO dao = new JogadorDAO();

        if (dao.update(player)) {
            System.out.println("O jogador foi atualizado com sucesso!");
        } else {
            System.out.println("Erro ao atualizar!");
        }
    }
    /**
     * Metodo para apagar um jogador
     */
    public static void PlayerDelete() {
        Jogador player = new Jogador();
        player.setNome_jogador("Marega");

        JogadorDAO dao = new JogadorDAO();

        if (dao.delete(player)) {
            System.out.println("O jogador foi eliminado com sucesso!");
        } else {
            System.out.println("Erro ao eliminar o jogador");
        }
    }

    public static void playerList() {
        JogadorDAO dao = new JogadorDAO();

        for (Jogador c : dao.findClubeA()) {

            System.out.println("Nome do Jogador: " + c.getNome_jogador() + " Numero Jogador: " + c.getNumero_jogador());
        }
    }

}
