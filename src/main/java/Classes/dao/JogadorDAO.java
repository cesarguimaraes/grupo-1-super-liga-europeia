package Classes.dao;

import Classes.Jogador;
import Connect.ConnectionBd;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JogadorDAO {
    /**
     * @param con cria a variavel para realizar a conexão a base de dados
     */
    private Connection con;
    /**
     * liga a base de dados
     */
    public JogadorDAO(){
    }

    /**
     * Metodo para inserir um Jogador
     * @param player tipo Jogador
     */
    public boolean save(Jogador player) {
        String sql = "INSERT INTO Jogador(nome_jogador, numero, id_clube) VALUES (?,?,?)";
        PreparedStatement stmt = null;

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            stmt.setString(1,player.getNome_jogador());
            stmt.setInt(2, player.getNumero_jogador());
            stmt.setInt(3, player.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para inserir um jogador
     */
    public List<Jogador> findIDJogador() {
        String sql = "SELECT id_jogador, id_clube FROM Jogador WHERE nome_jogador='"+ Controller.getInstance().getNomeJogador()+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Jogador> jogadores = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Jogador jogador = new Jogador();
                jogador.setId_jogador(rs.getInt("id_jogador"));
                jogador.setId_clube(rs.getInt("id_clube"));
                jogadores.add(jogador);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return jogadores;
    }

    /**
     * Metodo para pesquisar jogadores
     * @param id_jogador variavel tipo int
     */
    public List<Jogador> findNomeJogador(int id_jogador) {
        String sql = "SELECT nome_jogador FROM Jogador WHERE id_jogador="+id_jogador;

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Jogador> jogadores = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Jogador jogador = new Jogador();
                jogador.setNome_jogador(rs.getString("nome_jogador"));
                jogadores.add(jogador);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return jogadores;
    }

    /**
     * Metodo para pesquisar o jogador no clube da casa
     */
    public List<Jogador> findClubeA() {
        String sql = "SELECT nome_jogador, numero FROM Jogador INNER JOIN Clube ON Jogador.id_clube = Clube.id_clube WHERE Clube.nome_clube = '"+ Controller.getInstance().getClubeA()+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Jogador> jogadores = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Jogador jogador = new Jogador();
                jogador.setNome_jogador(rs.getString("nome_jogador"));
                jogador.setNumero_jogador(rs.getInt("numero"));
                jogadores.add(jogador);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return jogadores;
    }

    /**
     * Metodo para pesquisar o jogador no clube do visitante
     */
    public List<Jogador> findClubeB() {

        String sql = "SELECT nome_jogador, numero FROM Jogador INNER JOIN Clube ON Jogador.id_clube = Clube.id_clube WHERE Clube.nome_clube = '"+ Controller.getInstance().getClubeB()+"'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Jogador> jogadores = new ArrayList<>();

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Jogador jogador = new Jogador();
                jogador.setNome_jogador(rs.getString("nome_jogador"));
                jogador.setNumero_jogador(rs.getInt("numero"));
                jogadores.add(jogador);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do jogador" + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return jogadores;
    }

    /**
     * Metodo para modificar o jogador
     * @param player variavel tipo jogador
     */
    public boolean update(Jogador player) {
        String sql = "UPDATE Jogador SET nome_jogador = ? WHERE num_jogador = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, player.getNome_jogador());
            stmt.setInt(2, player.getNumero_jogador());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogador" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para apagar um jogador
     * @param player variavel tipo Jogador ira retornar o nome_jogador e o numero
     */
    public boolean delete(Jogador player) {
        String sql = "DELETE FROM Jogador WHERE nome_jogador = ? AND numero = ?";

        PreparedStatement stmt = null;

        try {
            con = ConnectionBd.establishConnection();
            stmt = con.prepareStatement(sql);
            stmt.setString(1,player.getNome_jogador());
            stmt.setInt(2,player.getNumero_jogador());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogador" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    public static void PlayerInsert() {
        Jogador player = new Jogador("Antonio Mendes", "Arsenal", 22);
        JogadorDAO dao = new JogadorDAO();

        if (dao.save(player)) {
            System.out.println("O Jogador foi adicionado com sucesso!");
        } else {
            System.out.println("Erro ao guardar o jogador na base de dados!");
        }
    }
    public static void PlayerUpdate() {
        Jogador player = new Jogador("Marega", "Sporting", 25);
        player.setNumero_jogador(1);
        JogadorDAO dao = new JogadorDAO();

        if (dao.update(player)) {
            System.out.println("O jogador foi atualizado com sucesso!");
        } else {
            System.out.println("Erro ao atualizar!");
        }
    }
    public static void PlayerDelete() {
        Jogador player = new Jogador();
        player.setNome_jogador("Marega");

        JogadorDAO dao = new JogadorDAO();

        if (dao.delete(player)) {
            System.out.println("O jogador foi eliminado com sucesso!");
        } else {
            System.out.println("Erro ao eliminar o jogador");
        }
    }

    public static void playerList() {
        JogadorDAO dao = new JogadorDAO();

        for (Jogador c : dao.findClubeA()) {

            System.out.println("Nome do Jogador: " + c.getNome_jogador() + " Numero Jogador: " + c.getNumero_jogador());
        }
    }

}
