package Classes.dao;

import Classes.Clube;
import Classes.Estadio;
import Connect.ConnectionBd;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EstadioDAO {
    /**
     * @param con cria a variavel para realizar a conexão a base de dados
     */
    private Connection con;
    /**
     * liga a base de dados
     */
    public EstadioDAO() {
        con = ConnectionBd.establishConnection();
    }

    /**
     * Metodo para inserir no clube
     * @param localizacao_do_jogo a variavel que guardara a informaçao do tipo clube
     */
    public boolean save(Estadio localizacao_do_jogo) {
        String sql = "INSERT INTO Clube(nome_clube, estadio, cidade, pais) VALUES (?,?,?,?)";
        PreparedStatement stmt = null;

        try {
            /*
        }
            stmt = con.prepareStatement(sql);

            stmt.setString(1, clube.getNome_clube());
            stmt.setString(2, clube.getEstadio());
            stmt.setString(3, clube.getCidade());
            stmt.setString(4, clube.getPais());
             */
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para pesquisar os nomes dos clubes a partir do estadio
     */
    public ArrayList<Clube> findClubeEstadio() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT nome_clube FROM Clube\n" +
                "INNER JOIN Estadio\n" +
                "ON Clube.id_estadio = Estadio.id_estadio\n" +
                "WHERE Clube.nome_clube = '" + Controller.getInstance().getClubeA() + "'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Clube> listdeclubes = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Clube club = new Clube();
                club.setNome_clube(rs.getString("nome_clube"));
                listdeclubes.add(club);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Estadio " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listdeclubes;
    }

    /**
     * Metodo para pesquisar o nome do estadio, o pais e a cidade respetiva do mesmo
     */
    public ArrayList<Estadio> findLocalEstadio() {
        con = ConnectionBd.establishConnection();
        String sql = "SELECT nome_estadio, pais, cidade FROM Estadio WHERE id_estadio = "+Controller.getInstance().getAuxEstadioID();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Estadio> listaEstadios = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()){
                Estadio e = new Estadio();
                e.setEstadio(rs.getString("nome_estadio"));
                e.setPais(rs.getString("pais"));
                e.setCidade(rs.getString("cidade"));
                listaEstadios.add(e);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Estadio " + e.getMessage());
        } finally {
            ConnectionBd.closeConnection(con, stmt, rs);
        }
        return listaEstadios;
    }

    public boolean update(Estadio estadio) {
        String sql = "UPDATE Estadio ...";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            //stmt.setString(1, clube.getNome_clube());
            //stmt.setInt(2, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Estadio " + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Metodo para pesquisar os nomes dos clubes
     */
    public boolean delete(Clube clube) {
        String sql = "DELETE FROM clube WHERE nome_clube = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,clube.getNome_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Estadio " + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }
   /*
    public static void clubInsert() {
        Clube club = new Clube("Braga", "Estádio Emirates", "Londres", "Inglaterra");
        ClubeDAO dao = new ClubeDAO();

        if (dao.save(club)) {
            System.out.println("O clube foi adicionado com sucesso!");
        } else {
            System.out.println("Erro ao guardar o clube na base de dados!");
        }
    }
    public static void clubUpdate() {
        Clube club = new Clube("Sporting", "Estádio Emirates", "Londres", "Inglaterra");
        club.setId_clube(2);
        ClubeDAO dao = new ClubeDAO();

        if (dao.update(club)) {
            System.out.println("O clube foi atualizado com sucesso!");
        } else {
            System.out.println("Erro ao atualizar!");
        }
    }
    public static void clubDelete() {
        Clube club = new Clube();
        club.setNome_clube("Braga");

        ClubeDAO dao = new ClubeDAO();

        if (dao.delete(club)) {
            System.out.println("O clube foi eliminado com sucesso!");
        } else {
            System.out.println("Erro ao eliminar o clube");
        }
    }

    public static void clubList() {
        ClubeDAO dao = new ClubeDAO();

        for (Clube c : dao.findClube()) {

            System.out.println("Nome do Clube: " + c.getNome_clube() + " Estadio: " + c.getEstadio() + " Cidade: " + c.getCidade() + " Pais: " + c.getPais());
        }
    }

     */
}

