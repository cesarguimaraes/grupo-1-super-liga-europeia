package Classes.dao;

import Classes.Jogo;
import Classes.Jornada;
import Connect.ConnectionBd;
import superliga.trabalho2_grupo1_superliga.Controllers.AdminJogos;
import superliga.trabalho2_grupo1_superliga.Controllers.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class JornadaJogoDAO {
    private Connection con = null;
    public JornadaJogoDAO() {
        con = ConnectionBd.establishConnection();
    }

    /**
     * Inserir jogos na base de dados
     * @sql variavel tipo String que guarda do inserte a ser usado
     * @stmt variavel que insere na base de dados
     */
    public boolean insertJornadaJogo( int i, int j) {
        System.out.println(i);
        String testeinner = Arrays.toString(Controller.getInstance().getJornadaAtual().
                        split("[a-z]"))
                .replace(",","")
                .replace("J","")
                .replace("[","")
                .replace("]","").trim();
        i= Integer.parseInt(testeinner);
        System.out.println(i);

        String sql = "INSERT INTO JornadaJogo (id_jornada, id_jogo)" +
                " VALUES (?,?)";
        PreparedStatement stmt = null;
        try {
            System.out.println(j);
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, i);
            stmt.setInt(2, j);

            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogo " + e.getMessage());
            return false;
        } finally {
            ConnectionBd.closeConnection(con, stmt);
        }
    }

    /**
     * Select dos jogos na base de dados
     * @sql variavel tipo String que guarda do inserte a ser usado
     * @stmt variavel que pesquisa na base de dados
     */
    public ArrayList<Jogo> findJogo(/*int idjornada*/){
        int coiza;
        String testeinner = Arrays.toString(Controller.getInstance().getJornadaAtual().
                        split("[a-z]"))
                .replace(",","")
                .replace("J","")
                .replace("[","")
                .replace("]","").trim();
        coiza= Integer.parseInt(testeinner);
        System.out.println(coiza);

        String sql = "select desc_jogo, nome_clubeA, nome_clubeB " +
                "from Jogo inner join JornadaJogo " +
                "on Jogo.id_jogo=JornadaJogo.id_jogo and JornadaJogo.id_jornada= "+coiza;

        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Jogo> listdejogos = new ArrayList<>();

        try {

            stmt = con.prepareStatement(sql);

            rs = stmt.executeQuery();

            while (rs.next()){
                Jogo jog = new Jogo();
                jog.setDesc_jogo(rs.getString("desc_jogo").trim());
                jog.setNome_clubeA(rs.getString("nome_clubeA").trim());
                jog.setNome_clubeB(rs.getString("nome_clubeB").trim());
                listdejogos.add(jog);
            }

        } catch (SQLException e) {
            System.err.println("Erro na base dados do Jogo" +e.getMessage());
        }finally {
            ConnectionBd.closeConnection(con,stmt,rs);
        }


        return listdejogos;
    }
/*
    public boolean update(Jogo clube){
        String sql = "UPDATE Jogo SET nome_clube = ? WHERE id_clube = ?";
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1,clube.getNome_clube());
            stmt.setInt(2, clube.getId_clube());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }
     */



    /**
     * Delete dos jogos na base de dados
     * @sql variavel tipo String que guarda do delete a ser usado
     * @stmt variavel que apaga da base de dados
     */
    public boolean delete(Jogo jogo){
        String sql = "DELETE FROM clube WHERE id_clube = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, jogo.getId_jogo());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Erro na base dados do Clube" +e.getMessage());
            return false;
        }finally {
            ConnectionBd.closeConnection(con,stmt);
        }
    }

}
