package Classes;

public class Evento {

    private int id_evento;
    private String desc_evento;
    private int minuto;
    private int id_jogo;
    private int id_clube;
    private int id_jogador;


    /**
     * Construtor completo
     * @param id_evento
     * @param desc_evento
     * @param minuto
     * @param id_jogo
     * @param id_clube
     * @param id_jogador
     */
    public Evento(int id_evento, String desc_evento, int minuto, int id_jogo, int id_clube, int id_jogador) {
        this.id_evento = id_evento;
        this.desc_evento = desc_evento;
        this.minuto = minuto;
        this.id_jogo = id_jogo;
        this.id_clube = id_clube;
        this.id_jogador = id_jogador;
    }

    /**
     * Construtor padrão
     */
    public Evento(){

    }

    //gets e sets
    /**
     * Devolve a descricao do evento
     *
     * @return descricao do evento
     */
    public int getId_evento() {
        return id_evento;
    }

    /**
     * Modifica a descricao do evento
     *
     * @param id_evento decicao do evento
     */
    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

    public String getDesc_evento() {
        return desc_evento;
    }

    public void setDesc_evento(String desc_evento) {
        this.desc_evento = desc_evento;
    }

    /**
     * Devolve a tempo do evento
     * @return tempo do jogo q ocorre o evento
     */
    public int getMinuto() {
        return minuto;
    }

    /**
     * Modifica a descricao do evento
     * @param minuto decicao do evento
     */
    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    /**
     * Devolve a descricao do evento
     * @return descricao do evento
     */
    public int getId_jogo() {
        return id_jogo;
    }
    /**
     * Modifica a descricao do evento
     * @param id_jogo decicao do evento
     */
    public void setId_jogo(int id_jogo) {
        this.id_jogo = id_jogo;
    }


    /**
     * Devolve a descricao do evento
     *
     * @return descricao do evento
     */
    public int getId_clube() {
        return id_clube;
    }
    /**
     * Modifica a descricao do evento
     *
     * @param id_clube decicao do evento
     */
    public void setId_clube(int id_clube) {
        this.id_clube = id_clube;
    }


    /**
     * Devolve a descricao do evento
     *
     * @return descricao do evento
     */
    public int getId_jogador() {
        return id_jogador;
    }
    /**
     * Modifica a descricao do evento
     *
     * @param id_jogador decicao do evento
     */
    public void setId_jogador(int id_jogador) {
        this.id_jogador = id_jogador;
    }


    @Override
    public String toString() {
        return "Evento: {" + "Tempo=" + minuto ;}
}
