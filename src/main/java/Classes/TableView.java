package Classes;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class TableView {
    private final SimpleStringProperty nome;
    private final SimpleIntegerProperty num;


    public TableView(String NomeA, int NumA) {
        this.nome = new SimpleStringProperty(NomeA);
        this.num = new SimpleIntegerProperty(NumA);
    }

    public String getNome() {
        return nome.get();
    }
    public void setNome(String Nome) {
        nome.set(Nome);
    }

    public int getNum() {
        return num.get();
    }
    public void setNum(int NumA) {
        num.set(NumA);
    }

}