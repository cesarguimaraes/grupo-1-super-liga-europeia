module superliga.trabalho2_grupo1_superliga {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    requires org.kordamp.bootstrapfx.core;
    requires java.datatransfer;
    requires java.desktop;

    opens Classes;

    opens superliga.trabalho2_grupo1_superliga to javafx.fxml;
    opens superliga.trabalho2_grupo1_superliga.Controllers to javafx.fxml;
    exports superliga.trabalho2_grupo1_superliga;

}