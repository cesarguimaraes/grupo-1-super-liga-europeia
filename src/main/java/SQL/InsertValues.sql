﻿
INSERT INTO Estadio VALUES('Estádio do Dragão', 'Porto', 'Portugal'),
						  ('Estádio da Luz', 'Lisboa', 'Portugal'),
						  ('Estádio José Avalade', 'Lisboa', 'Portugal'),
						  ('Estádio Municipal de Braga', 'Braga', 'Portugal'),
						  ('Estádio Cidade de Barcelos', 'Barcelos', 'Portugal'),
						  ('Estádio D. Afonso Henrriques', 'Guimarães', 'Portugal'),
						  ('estádio municipal da capital do móvel ', 'Passos de Portugal', 'Inglaterra'),
						  ('Estádio os Barreiros', 'Funchal', 'Portugal'),
						  ('Anfield', 'Milao', 'Italia'),
						  ('Etihad Stadium', 'Londres', 'Inglaterra'),
						  ('Estádio Old Trafford', 'Manchester', 'Reino Unido'),
						  ('Estádio Santiago Bernabéu', 'Madrie', 'Espanha'),
						  ('New Tottenham Hotspur', 'Londres', 'Inglaterra'),
						  ('Giuseppe Meazza', 'Milão ', 'Italia'),
						  ('Estádio Emirates', 'Londres', 'Inglaterra'),
						  ('Wanda Metropolitano', 'Madrid', 'Espanha'),
						  ('Stamford Bridge', 'Londres', 'Inglaterra'),
						  ('Camp Nou', 'Barcelona', 'Espanha'),
						  ('Estádio Giuseppe Meazza', 'Milão', 'Italia'),
						  ('Juventus Stadium', 'Turim', 'Italia')

--Alguma tabela tem que não ter FOREIGN KEY ou não poderá ser criada
--Feito na tabela Clube e Jogo
ALTER TABLE Clube
ADD CONSTRAINT fk_Clube_id_estadio FOREIGN KEY (id_estadio) REFERENCES Estadio(id_estadio)
go
ALTER TABLE Jogo
ADD CONSTRAINT fk_Jornada_id_jogo FOREIGN KEY (id_jogo) REFERENCES Jogo(id_jogo)

--CLUBES
INSERT INTO Clube VALUES('Porto',0, 1),
							('Benfica',0, 2),
							('Sporting', 0, 3),
							('Braga',0,4),
							('Gil Vicente', 0,5),
							('Vitoria SC', 0,6),
							('P. Ferreira', 0,7),
							('Maritímo', 0, 8),
							('Liverpool', 0,9),
							('Manchester City', 0, 10),
							('Manchester United', 0, 11),
							('Real Madrid', 0, 12),
							('Tottenham', 0,13),
							('AC Milan',0,14),
							('Arsenal', 0,15),
							('Atlético de Madrid',0, 16),
							('Chelsea',0, 17),
							('Barcelona',0, 18),
							('Inter Milão',0, 19),
							('Juventus',0,20)

--JOGOS
select * from Jogo
INSERT INTO Jogo VALUES('jogo1',1,2,1,'Porto','Benfica'),
						('jogo2',3,4,3,'Sporting','Braga'),
						('jogo3',5,6,5,'Gil Vicente','Vitoria SC')

--JOGADORES
select * from Jogador
--insert no Porto
INSERT INTO Jogador VALUES('Diogo Costa', 99, 1),
						  ('Pepê', 11, 1),
						  ('C. Mbemba', 19, 1),
						  ('Pepe', 3, 1),
						  ('Emerson', 12, 1),
						  ('Z. Sanusi', 12, 1),
						  ('Otávio', 25, 1),
						  ('M. Grujic', 16, 1),
						  ('M. Taremi' ,9, 1),
						  ('Fábio Vieira', 50, 1),
						  ('Evanilson', 30, 1)
--insert no Benfica
INSERT INTO Jogador VALUES('D. Núñez', 9, 2),
						  ('D. Gonçalves', 17, 2),
						  ('G. Ramos', 88, 2),
						  ('Everton', 7, 2),
						  ('J. Weigl', 28, 2),
						  ('A. Taarabt', 49, 2),
						  ('Álex Grimaldo', 3, 2),
						  ('J.Vertonghen', 5, 2),
						  ('N. Otamendi', 30, 2),
						  ('Gilberto', 2, 2),
						  ('O. Vlachodimos', 99, 2)
--insert no Sporting
INSERT INTO Jogador VALUES('Adán', 1, 3),
						  ('L. Neto', 13, 3),
						  ('S. Coates', 4, 3),
						  ('Gonçalo Inácio', 25, 3),
						  ('Pedro Porro', 24, 3),
						  ('J. Palhinha', 6, 3),
						  ('Matheus Luiz', 8, 3),
						  ('J.Vertonghen', 5, 3),
						  ('N. Santos', 11, 3),
						  ('Pablo Sarabia', 17, 3),
						  ('Pedro Gonçalves', 28, 3)
--insert no Braga
INSERT INTO Jogador VALUES('R. Horta', 21, 4),
						  ('Abel Ruiz', 9, 4),
						  ('R. Gomes', 57, 4),
						  ('A. Horta', 10, 4),
						  ('Al-Musrati', 8, 4),
						  ('Castro', 88, 4),
						  ('Fabiano', 70, 4),
						  ('V. Tormena', 3, 4),
						  ('D. Carmo', 16, 4),
						  ('P. Oliveira', 15, 4),
						  ('Matheus', 1, 4)
--insert no Gil Vicente
INSERT INTO Jogador VALUES('Z. Frelih', 1, 5),
						  ('Z. C', 2, 5),
						  ('Lucas Cunha', 3, 5),
						  ('R. Fernades', 26, 5),
						  ('Talocha', 31, 5),
						  ('Pedrinho', 8, 5),
						  ('Carvalho', 21, 5),
						  ('A. Léautey', 11, 5),
						  ('K. Fujimoto', 10, 5),
						  ('S. Lino', 29, 5),
						  ('Fran Navarro', 9, 5)
--insert no Vitoria SC
INSERT INTO Jogador VALUES('B. Varela', 14, 6),
						  ('M. Maga', 62, 6),
						  ('J. Fernandes', 44, 6),
						  ('A. Mumin', 6, 6),
						  ('Rafa', 5, 6),
						  ('André Almeida', 70, 6),
						  ('I. Bamba', 41, 6),
						  ('N. Janvier', 98, 6),
						  ('Ruben Lambeiras', 8, 6),
						  ('O. Estupiñán', 19, 6),
						  ('Rochinha', 16, 6)
--insert no Passos de Ferreira
INSERT INTO Jogador VALUES('André Ferreira', 13, 7),
						  ('J. Delgado', 15, 7),
						  ('N. Lima', 3, 7),
						  ('Maracás', 6, 7),
						  ('L. Bastos', 20, 7),
						  ('L. Carlos', 22, 7),
						  ('R. Pires', 26, 7),
						  ('Nuno Santos', 77, 7),
						  ('Hélder Ferreira', 7, 7),
						  ('A. Butzke', 17, 7),
						  ('Zé Uilton', 9, 7)
--insert no Maritimo
INSERT INTO Jogador VALUES('Joel', 95, 8),
						  ('A. Vídigal', 7, 8),
						  ('M. Ferreira de Sousa', 36, 8),
						  ('R. Guitane', 8, 8),
						  ('P. Pelágio', 60, 8),
						  ('I. Rossi', 15, 8),
						  ('Vítor', 94, 8),
						  ('Z. Júnior', 5, 8),
						  ('M. Costa', 4, 8),
						  ('C. Winck', 2, 8),
						  ('P. Víctor', 48, 8)
--insert do Liverpool
INSERT INTO Jogador VALUES('L. Díaz', 23, 9),
						  ('S.mané', 10, 9),
						  ('Mohamed Salah', 11, 9),
						  ('Thiago', 6, 9),
						  ('Fabinho', 3, 9),
						  ('K. Keita', 8, 9),
						  ('A. Robertson', 26, 9),
						  ('V. van Dijk', 4, 9),
						  ('I. Konaté', 5, 9),
						  ('T. Alexander Arnold', 66, 9),
						  ('Alisson', 1, 9)
--insert do Manchester City
INSERT INTO Jogador VALUES('Ederson Moraes',31, 10),
						  ('Kyle Walker', 2, 10),
						  ('John Stones', 5, 10),
						  ('Aymeric Laporte', 14, 10),
						  ('João Cancelo', 27, 10),
						  ('Kevin De Bryyne', 17, 10),
						  ('Rodri', 16, 10),
						  ('İlkay Gündoğan', 8, 10),
						  ('Riyad Mahrez', 26, 10),
						  ('Phill Foden', 47, 10),
						  ('Bernado Silva', 20, 10)
--insert do Manchester United
INSERT INTO Jogador VALUES('D. de Gea', 1, 11),
						  ('Diogo Dalot', 20, 11),
						  ('H.Maguire', 5, 11),
						  ('V.Lindelöf', 2, 11),
						  ('B.Fernandes', 18, 11),
						  ('P.Pogba', 6, 11),
						  ('A.Telles', 27, 11),
						  ('J.Lingard', 14, 11),
						  ('A.Elanga', 36, 11),
						  ('C. Ronaldo', 7, 11),
						  ('J.Sancho', 25, 11)
--insert do Real Madrid
INSERT INTO Jogador VALUES('T.Courtois', 1, 12),
						  ('Carvajal', 2, 12),
						  ('Éder Militão', 3, 12),
						  ('Nacho', 6, 12),
						  ('F.Mendy', 23, 12),
						  ('Casemiro', 14, 12),
						  ('T. Kroos', 8, 12),
						  ('L.Modrić',10, 12),
						  ('C. Ronaldo', 7, 12),
						  ('Vinícius Júnior', 20, 12),
						  ('K. Benzema', 9, 12)
--insert do Tottenham
INSERT INTO Jogador VALUES('H.Lloris', 1, 13),
						  ('C. Romero', 4, 13),
						  ('E. Dier', 15, 13),
						  ('B. Davies', 33, 13),
						  ('Emerson', 12, 13),
						  ('D. Kulusevskj', 21, 13),
						  ('R. Bentancur', 30, 13),
						  ('P. Højbjerg', 5, 13),
						  ('Reguilón', 3, 13),
						  ('H. Kane', 10, 13),
						  ('H. Son',7, 13)
--insert do AC Milan
INSERT INTO Jogador VALUES('Brahim Diaz', 10, 14),
						  ('Llorente', 14, 14),
						  ('S. Savić', 15, 14),
						  ('Filipe', 18, 14),
						  ('Reinildo', 23, 14),
						  ('Renan Lodi', 12, 14),
						  ('A .Griezmann', 8, 14),
						  ('Kike', 6, 14),
						  ('G. Kondogbia', 4, 14),
						  ('T. Lemar', 11, 14),
						  ('João Félix', 7, 14)  
--insert do Arsenal
INSERT INTO Jogador VALUES('Aaron Ramsdale', 32, 15),
						  ('Nuno Tavares', 20, 15),
						  ('Gabriel', 6, 15),
						  ('Ben White', 4, 15),
						  ('Cédric Soares', 17, 15),
						  ('Albert Lokonga', 23, 15),
						  ('Granit Xhaka', 34, 15),
						  ('Bukayo Saka', 7, 15),
						  ('Martin Ødegaard', 8, 15),
						  ('Gabriel Martinelli', 35, 15),
						  ('Eddie Nketiah', 30, 15)
--insert do Atlectico Madrid
INSERT INTO Jogador VALUES('J. Oblak', 13, 16),
						  ('Llorente', 14, 16),
						  ('S. Savić', 15, 16),
						  ('Filipe', 18, 16),
						  ('Reinildo', 23, 16),
						  ('Renan Lodi', 12, 16),
						  ('A .Griezmann', 8, 16),
						  ('Kike', 6, 16),
						  ('G. Kondogbia', 4, 16),
						  ('T. Lemar', 11, 16),
						  ('João Félix', 7, 16)
						  select * from Jogador
--insert do Chelsea
INSERT INTO Jogador VALUES('Edouard Mendy', 16, 17),
						  ('Reece James', 24, 17),
						  ('Andreas Christensen',4, 17),
						  ('Antonio Rüdiger', 2, 17),
						  ('César Azpilicueta', 28, 17),
						  ('Jorginho', 5, 17),
						  ('Mateo Kovačić', 8, 17),
						  ('Marcos Alonso', 3, 17),
						  ('Mason Mount', 19, 17),
						  ('Kai Havertz', 29, 17),
						  ('Time Werner', 11, 17)
--insert do Barcelona
INSERT INTO Jogador VALUES('Marc Stegen ',1, 18),
						  ('Dani Alves', 8, 18),
						  ('Ronald Araujo',4, 18),
						  ('Gerard Piqué',3, 18),
						  ('Jordi Alba',18, 18),
						  ('Frenkie de Jong', 21, 18),
						  ('Sergio Busquets', 5, 18),
						  ('Marcos Alonso', 3, 18),
						  ('Ferran Torres', 19, 18),
						  ('Pierre Aubameyang', 25, 18),
						  ('Memphis Depay', 9, 18)

--insert do inter Milao
INSERT INTO Jogador VALUES(' E. Džeko ', 9, 19),
						  ('L. Martínez ', 10, 19),
						  ('D. Dumfries', 2, 19),
						  ('N. Barella', 23, 19),
						  ('M. Brozović', 77, 19),
						  ('H. Çalhanoğlu', 20, 19),
						  ('I. Perišić', 14, 19),
						  ('M. Škriniar',37, 19),
						  ('S. Vrij', 6, 19),
						  ('F. Dimarco', 32, 19),
						  ('S. Handanovič', 1, 19)
--insert do Juventus
INSERT INTO Jogador VALUES('Marc Stegen ',1, 20),
						  ('Dani Alves', 8, 20),
						  ('Ronald Araujo',4, 20),
						  ('Gerard Piqué',3, 20),
						  ('Jordi Alba',18, 20),
						  ('Frenkie de Jong',4, 20),
						  ('Sergio Busquets', 5, 20),
						  ('Marcos Alonso', 3, 20),
						  ('Ferran Torres', 19, 20),
						  ('Pierre Aubameyang', 25, 20),
						  ('Memphis Depay', 9, 20)



--EVENTOS DO JOGO
select * from Jogo

SELECT num_jogo, id_clubeA, id_clubeB FROM Jogo

ALTER TABLE TipoEvento
ADD CONSTRAINT fk_TipoEvento_id_evento FOREIGN KEY (id_evento) REFERENCES Eventos_jogo(id_tipo_evento)
ALTER TABLE Eventos_jogo
ADD CONSTRAINT fk_Eventos_id_jogo FOREIGN KEY (id_jogo) REFERENCES Jogo(id_jogo)

--JORNADAS
INSERT INTO Jornada VALUES('Jornada 1')
INSERT INTO Jornada VALUES('Jornada 2')
INSERT INTO Jornada VALUES('Jornada 3')
go
INSERT INTO Jornada VALUES('Jornada 4')
INSERT INTO Jornada VALUES('Jornada 5')
INSERT INTO Jornada VALUES('Jornada 6')
go
INSERT INTO Jornada VALUES('Jornada 7')
INSERT INTO Jornada VALUES('Jornada 8')
INSERT INTO Jornada VALUES('Jornada 9')
go

--JOGOS POR DEFEITO
select * from Jogo
select * from Jornada
SELECT  desc_jornada FROM Jornada ORDER BY desc_jornada	

--Jornada 1
INSERT INTO Jogo VALUES('Jogo 1', 1, 5, 1),
					   ('Jogo 2', 2, 6, 1),
					   ('Jogo 3', 3, 4, 1)
					   --Jornada 2
INSERT INTO Jogo VALUES('Jogo 1', 5, 4, 2),
                       ('Jogo 2', 6, 3, 2),
					   ('Jogo 3', 1, 2, 2)
					   --Jornada 3
INSERT INTO Jogo VALUES('Jogo 1', 2, 5, 3),
					   ('Jogo 2', 3, 1, 3),
					   ('Jogo 3', 4, 6, 3)
					   --Jornada 4
INSERT INTO Jogo VALUES('Jogo 1', 5, 6, 4),
					   ('Jogo 2', 1, 4, 4),
					   ('Jogo 3', 2, 3, 4)
					   --Jornada 5
INSERT INTO Jogo VALUES('Jogo 1', 3, 5, 5),
					   ('Jogo 2', 4, 2, 5),
					   ('Jogo 3', 5, 1, 5)
					   --Jornada 6
INSERT INTO Jogo VALUES('Jogo 1', 5, 1, 5),
					   ('Jogo 2', 6, 2, 5),
					   ('Jogo 3', 4, 3, 5)
					   --Jornada 7
INSERT INTO Jogo VALUES('Jogo 1', 3, 5, 5),
					   ('Jogo 2', 4, 2, 5),
					   ('Jogo 3', 5, 1, 5)
					   --Jornada 8
INSERT INTO Jogo VALUES('Jogo 1', 5, 2, 5),
					   ('Jogo 2', 1, 3, 5),
					   ('Jogo 3', 6, 4, 5)
					   --Jornada 9
INSERT INTO Jogo VALUES('Jogo 1', 3, 5, 5),
					   ('Jogo 2', 4, 2, 5),
					   ('Jogo 3', 5, 1, 5)
					   --Jornada 10
INSERT INTO Jogo VALUES('Jogo 1', 6, 5, 5),
					   ('Jogo 2', 2, 4, 5),
					   ('Jogo 3', 1, 6, 5)

Insert Into JornadaJogo Values(1,8),
								(1,9)

								select * from JornadaJogo
							   --Arsenal
INSERT INTO Eventos_jogo VALUES('0-0', 0, 1, 1, 1),
							   ('0-0', 0, 1, 1, 2),
							   ('0-0', 0, 1, 1, 3),
							   ('0-0', 0, 1, 1, 4),
							   ('0-0', 0, 1, 1, 5),
							   ('0-0', 0, 1, 1, 6),
							   ('0-0', 0, 1, 1, 7),
							   ('0-0', 0, 1, 1, 8),
							   ('0-0', 0, 1, 1, 9),
							   ('0-0', 0, 1, 1, 10),
							   ('0-0', 0, 1, 1, 11)

							   --Atletico Madrid
INSERT INTO Eventos_jogo VALUES('0-0', 0, 1, 1, 12),
							   ('0-0', 0, 1, 1, 13),
							   ('0-0', 0, 1, 1, 14),
							   ('0-0', 0, 1, 1, 15),
							   ('0-0', 0, 1, 1, 16),
							   ('0-0', 0, 1, 1, 17),
							   ('0-0', 0, 1, 1, 18),
							   ('0-0', 0, 1, 1, 19),
							   ('0-0', 0, 1, 1, 20),
							   ('0-0', 0, 1, 1, 21),
							   ('0-0', 0, 1, 1, 22)
							   --Chelsea
INSERT INTO Eventos_jogo VALUES('0-0', 0, 1, 1, 23),
							   ('0-0', 0, 1, 1, 24),
							   ('0-0', 0, 1, 1, 25),
							   ('0-0', 0, 1, 1, 26),
							   ('0-0', 0, 1, 1, 27),
							   ('0-0', 0, 1, 1, 28),
							   ('0-0', 0, 1, 1, 29),
							   ('0-0', 0, 1, 1, 30),
							   ('0-0', 0, 1, 1, 31),
							   ('0-0', 0, 1, 1, 32),
							   ('0-0', 0, 1, 1, 33)
							   --Barcelona
INSERT INTO Eventos_jogo VALUES(0, 0, 0, 0, 0, '0-0', 4, 34),
							   (0, 0, 0, 0, 0, '0-0', 4, 35),
							   (0, 0, 0, 0, 0, '0-0', 4, 36),
							   (0, 0, 0, 0, 0, '0-0', 4, 37),
							   (0, 0, 0, 0, 0, '0-0', 4, 38),
							   (0, 0, 0, 0, 0, '0-0', 4, 39),
							   (0, 0, 0, 0, 0, '0-0', 4, 40),
							   (0, 0, 0, 0, 0, '0-0', 4, 41),
							   (0, 0, 0, 0, 0, '0-0', 4, 42),
							   (0, 0, 0, 0, 0, '0-0', 4, 43),
							   (0, 0, 0, 0, 0, '0-0', 4, 44)
							   --Inter Milao
INSERT INTO Eventos_jogo VALUES(0, 0, 0, 0, 0, '0-0', 5, 45),
							   (0, 0, 0, 0, 0, '0-0', 5, 46),
							   (0, 0, 0, 0, 0, '0-0', 5, 47),
							   (0, 0, 0, 0, 0, '0-0', 5, 48),
							   (0, 0, 0, 0, 0, '0-0', 5, 49),
							   (0, 0, 0, 0, 0, '0-0', 5, 50),
							   (0, 0, 0, 0, 0, '0-0', 5, 51),
							   (0, 0, 0, 0, 0, '0-0', 5, 52),
							   (0, 0, 0, 0, 0, '0-0', 5, 53),
							   (0, 0, 0, 0, 0, '0-0', 5, 54),
							   (0, 0, 0, 0, 0, '0-0', 5, 55)
							   --Juventus
INSERT INTO Eventos_jogo VALUES(0, 0, 0, 0, 0, '0-0', 6, 56),
							   (0, 0, 0, 0, 0, '0-0', 6, 57),
							   (0, 0, 0, 0, 0, '0-0', 6, 58),
							   (0, 0, 0, 0, 0, '0-0', 6, 59),
							   (0, 0, 0, 0, 0, '0-0', 6, 60),
							   (0, 0, 0, 0, 0, '0-0', 6, 61),
							   (0, 0, 0, 0, 0, '0-0', 6, 62),
							   (0, 0, 0, 0, 0, '0-0', 6, 63),
							   (0, 0, 0, 0, 0, '0-0', 6, 64),
							   (0, 0, 0, 0, 0, '0-0', 6, 65),
							   (0, 0, 0, 0, 0, '0-0', 6, 66)



							   select nome_clubeA, nome_clubeB from Jogo 
							   inner join JornadaJogo 
							   on Jogo.id_jogo=JornadaJogo.id_jornada and Jogo.id_jogo=1
							


GO
INSERT INTO user_account (firstname, lastname, username, password) VALUES ('Tiago', 'Ferraz', 'tiagoferraz', '12345')