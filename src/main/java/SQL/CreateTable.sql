--TABELA JORNADA
create table Jornada(
	id_jornada int IDENTITY (1,1) Primary key,
	desc_jornada nvarchar(15)
)

create table Estadio(
	id_estadio int IDENTITY (1,1) Primary key,
	nome_estadio nvarchar(50),
	pais nvarchar(50),
	cidade nvarchar(50),
)

go
create table Clube(
	id_clube int IDENTITY (1,1) Primary key,
	nome_clube nvarchar(50),
	pontos int,
	id_estadio int,
	CONSTRAINT fk_Clube_id_estadio FOREIGN KEY (id_estadio) REFERENCES Estadio(id_estadio),
)

go
create table Jogo(
	id_jogo int IDENTITY (1,1) Primary key,
	desc_jogo nvarchar(50),
	id_clubeA int,
	id_clubeB int,
	id_estadio int,
	CONSTRAINT fk_Jogo_id_clubeA FOREIGN KEY (id_clubeA) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Jogo_id_clubeB FOREIGN KEY (id_clubeB) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Jogo_id_estadio FOREIGN KEY (id_estadio) REFERENCES Clube(id_clube)
)

go
create table JornadaJogo(
	id_jornada int ,
	id_jogo int ,
	PRIMARY KEY (id_jornada,id_jogo),
	CONSTRAINT fk_JornadaJogo_id_jornada FOREIGN KEY (id_jornada) REFERENCES Jornada(id_jornada),
	CONSTRAINT fk_JornadaJogo_jogo FOREIGN KEY (id_jogo) REFERENCES Jogo(id_jogo)
)

create table Jogador(
	id_jogador int IDENTITY (1,1) Primary key,
	nome_jogador nvarchar(50),
	numero int,
	id_clube int,
	CONSTRAINT fk_Jogador_id_clube FOREIGN KEY (id_clube) REFERENCES Clube(id_clube),
)
go

create table Evento(
	id_evento int IDENTITY (1,1) Primary key,
	desc_evento varchar(250),
	minuto int,
	id_jogo int,
	id_clube int,
	id_jogador int,
	CONSTRAINT fk_Evento_id_jogo FOREIGN KEY (id_jogo) REFERENCES Jogo(id_jogo),
	CONSTRAINT fk_Evento_id_clube FOREIGN KEY (id_clube) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Evento_id_jogador FOREIGN KEY (id_jogador) REFERENCES Jogador(id_jogador)
)
go

create table Resultado(
	id_resultado int IDENTITY (1,1) Primary key,
	golosA int,
	golosB int,
	id_clubeA int,
	id_clubeB int,
	id_jogador int,
	CONSTRAINT fk_Evento_id_clubeB FOREIGN KEY (id_clubeA) REFERENCES Jogo(id_jogo),
	CONSTRAINT fk_Jogador_id_clubeB FOREIGN KEY (id_clubeB) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Jogador_id_jogador FOREIGN KEY (id_jogador) REFERENCES Jogador(id_jogador)
)
go

create table Tipo_estatistica(
	id_tipo int IDENTITY (1,1) Primary key,
	descricao nvarchar(50)
)
go

create table Estatistica(
	id_estatistica int IDENTITY (1,1) Primary key,
	qtd int,
	id_clube int,
	id_jogador int,
	id_tipo int,
	CONSTRAINT fk_Estatistica_id_clube FOREIGN KEY (id_clube) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Estatistica_id_jogador FOREIGN KEY (id_jogador) REFERENCES Jogador(id_jogador),
	CONSTRAINT fk_Estatistica_id_tipo FOREIGN KEY (id_tipo) REFERENCES Tipo_estatistica(id_tipo)
)

go

create table user_account(
	account_id int IDENTITY (1,1) Primary key,
	firstname VARCHAR(45) NOT NULL,
	lastname VARCHAR(45) NOT NULL,
	username VARCHAR(45) NOT NULL UNIQUE,
	password VARCHAR(45) NOT NULL,
	permission VARCHAR(45) NOT NULL
)