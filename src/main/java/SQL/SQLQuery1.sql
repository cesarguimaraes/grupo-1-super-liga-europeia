﻿Create database Superliga;

use Superliga;
go
create table Clube(
	id_clube int IDENTITY (1,1) Primary key,
	nome_clube varchar(35),
	estadio varchar(35),
	cidade varchar(35),
	pais varchar(35)
)

create table Jogador(
	id_jogador int IDENTITY (1,1) Primary key,
	nome_jogador varchar(35),
	num_jogador varchar(35),
	clube_jogador varchar(35),
	id_clube int,
	CONSTRAINT fk_Jogador_id_clube FOREIGN KEY (id_clube) REFERENCES Clube(id_clube)
)

create table Eventos_jogo(
	id_jogo int IDENTITY (1,1) Primary key,
	golos_A int,
	golos_B int,
	cartões_A int,
	cartões_B int,
	substituições_A int,
	substituições_B int,
	anulados_A int,
	anulados_B int,
	res_intervalo varchar(10),
	res_final varchar(10),
	id_clubeA int,
	id_clubeB int,
	CONSTRAINT fk_Eventos_id_clubeA FOREIGN KEY (id_clubeA) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Eventos_id_clubeB FOREIGN KEY (id_clubeB) REFERENCES Clube(id_clube)
)

create table Jornada(
	id_jornada int IDENTITY (1,1) Primary key,
	num_jornada varchar(35),
	id_jogo int,
	CONSTRAINT fk_Jornada_id_jogo FOREIGN KEY (id_jogo) REFERENCES Eventos_jogo(id_jogo)
)

create table Localização(
	id_localização int IDENTITY (1,1) Primary key,
	nome_clubeA varchar(35),
	nome_clubeB varchar(35),
	estadio varchar(35),
	cidade varchar(35),
	pais varchar(35),
	id_clube int,
	id_jogo int,
	CONSTRAINT fk_Localização_id_clube FOREIGN KEY (id_clube) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Localização_id_jogo FOREIGN KEY (id_jogo) REFERENCES Eventos_jogo(id_jogo)
)

create table Classificação(
	id_classificação int IDENTITY (1,1) Primary key,
	nome_clube varchar(35),
	pontos int,
	id_clube int,
	id_jogo int,
	CONSTRAINT fk_Classificação_id_clube FOREIGN KEY (id_jogo) REFERENCES Clube(id_clube),
	CONSTRAINT fk_Classificação_id_jogo FOREIGN KEY (id_jogo) REFERENCES Eventos_jogo(id_jogo)
)

create table Estatistica(
	id_estatistica int IDENTITY (1,1) Primary key,
	marcador_geral varchar(35),
	marcador_equipas varchar(35),
	equipas_derrotas varchar(35),
	id_jogador int,
	id_classificação int,
	id_jogo int,
	CONSTRAINT fk_Estatistica_id_jogador FOREIGN KEY (id_jogador) REFERENCES Jogador(id_jogador),
	CONSTRAINT fk_Estatistica_id_classificação FOREIGN KEY (id_estatistica) REFERENCES Classificação(id_classificação),
	CONSTRAINT fk_Estatistica_id_jogo FOREIGN KEY (id_jogo) REFERENCES Eventos_jogo(id_jogo)
)

--drop table Clube
--drop table Eventos_jogo
--drop table Classificação
--drop table Localização

-- equipas da super liga
INSERT INTO Clube VALUES('Arsenal','Estádio Emirates', 'Londres', 'Inglaterra'),
							('Atletico Madrid', 'Wanda Metropolitano', 'Madrid', 'Espanha'),
							('Chelsea', 'Stamford Bridge', 'Londres', 'Inglaterra'),
							('Barcelona','Camp Nou','Catalunha','Espanha'),
							('Inter Milão', 'Estádio Giuseppe Meazza','Milão','Italia'),
							('Juventus', 'Juventus Stadium','Turim','Italia'),
							('Liverpool', ' Anfield, AXA Training Centre','Milão','Italia'),
							('Manchester City', 'Etihad Stadium','Manchester', 'Reino Unido'),
							('Manchester United', 'Estádio Old Trafford','Manchester', 'Reino Unido'),
							('Real Madrid', 'Estádio Santiago Bernabéu','Madrid', 'Espanha'),
							('Tottenham', 'New Tottenham Hotspur','Londres','Reino Unido'),
							('AC Milan','Giuseppe Meazza','Milão','Italia')


-- equipas da liga portuguesa
INSERT INTO Clube VALUES('Porto','Estadio do Dragão', 'Porto', 'Portugal'),
							('Benfica', 'Estadio da Luz', 'Lisboa', 'Portugal'),
							('Sporting', 'Estádio José Alvalade', 'Lisboa', 'Portugal'),
							('Braga','Estádio Municipal de Braga','Braga','Portugal'),
							('Gil Vicente', 'Estádio Cidade de Barcelos','Barcelos','Portugal'),
							('Vitoria SC', 'Estádio D. Afonso Henriques','Guimarães','Portugal'),
							('P. Ferreira', 'E. M. da Capital do Móvel','Paços de Ferreira','Portugal'),
							('Maritímo', 'Estádio dos Barreiros','Madeira', 'Portugal')


Select * from Clube

--insert do inter Milao
INSERT INTO Jogador VALUES(' E. Džeko ', 9, 'Inter Milao', 17),
						  ('L. Martínez ', 10, 'Inter Milao', 17),
						  ('D. Dumfries', 2, 'Inter Milao', 17),
						  ('N. Barella', 23, 'Inter Milao', 17),
						  ('M. Brozović', 77, 'Inter Milao', 17),
						  ('H. Çalhanoğlu', 20, 'Inter Milao', 17),
						  ('I. Perišić', 14, 'Inter Milao', 17),
						  ('M. Škriniar',37, 'Inter Milao', 17),
						  ('S. Vrij', 6, 'Inter Milao', 17),
						  ('F. Dimarco', 32, 'Inter Milao', 17),
						  ('S. Handanovič', 1,'Inter Milao', 17)

Select Jogador.id_jogador, Jogador.nome_jogador from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 17

--insert do Atlectico madrid
INSERT INTO Jogador VALUES('J. Oblak', 13, 'Atletetico Madrid',25),
						  ('Llorente', 14, 'Atletetico Madrid',25),
						  ('S. Savić', 15, 'Atletetico Madrid',25),
						  ('Filipe', 18, 'Atletetico Madrid',25),
						  ('Reinildo', 23, 'Atletetico Madrid',25),
						  ('Renan Lodi', 12, 'Atletetico Madrid',25),
						  ('A .Griezmann', 8, 'Atletetico Madrid',25),
						  ('Kike', 6, 'Atletetico Madrid',25),
						  ('G. Kondogbia', 4, 'Atletetico Madrid',25),
						  ('T. Lemar', 11, 'Atletetico Madrid',25),
						  ('João Félix', 7,'Atletetico Madrid',25)

Select Jogador.id_jogador, Jogador.nome_jogador from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 25


--insert do AC Milan
INSERT INTO Jogador VALUES('Brahim Diaz', 10, ' AC Milan',1),
						  ('Llorente', 14, ' AC Milan',1),
						  ('S. Savić', 15, ' AC Milan',1),
						  ('Filipe', 18, ' AC Milan',1),
						  ('Reinildo', 23, ' AC Milan',1),
						  ('Renan Lodi', 12, ' AC Milan',1),
						  ('A .Griezmann', 8, ' AC Milan',1),
						  ('Kike', 6, ' AC Milan',1),
						  ('G. Kondogbia', 4, ' AC Milan',1),
						  ('T. Lemar', 11, ' AC Milan',1),
						  ('João Félix', 7,' AC Milan',1)

Select Jogador.id_jogador, Jogador.nome_jogador from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 1


--insert do Arsenal
INSERT INTO Jogador VALUES('Aaron Ramsdale', 32, 'Arsenal',13),
						  ('Nuno Tavares', 20, 'Arsenal',13),
						  ('Gabriel', 6, 'Arsenal',13),
						  ('Ben White', 4, 'Arsenal',13),
						  ('Cédric Soares', 17, 'Arsenal',13),
						  ('Albert Lokonga', 23, 'Arsenal',13),
						  ('Granit Xhaka', 34, 'Arsenal',13),
						  ('Bukayo Saka', 7, 'Arsenal',13),
						  ('Martin Ødegaard', 8, 'Arsenal',13),
						  ('Gabriel Martinelli', 35, 'Arsenal',13),
						  ('Eddie Nketiah', 30,'Arsenal',13 )

Select Jogador.id_jogador, Jogador.nome_jogador from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 13


--insert do Manchester City
INSERT INTO Jogador VALUES('Ederson Moraes',31, 'Manchester City',20),
						  ('Kyle Walker', 2, 'Manchester City',20),
						  ('John Stones', 5, 'Manchester City',20),
						  ('Aymeric Laporte', 14, 'Manchester City',20),
						  ('João Cancelo', 27, 'Manchester City',20),
						  ('Kevin De Bryyne', 17, 'Manchester City',20),
						  ('Rodri', 16, 'Manchester City',20),
						  ('İlkay Gündoğan', 8, 'Manchester City',20),
						  ('Riyad Mahrez', 26, 'Manchester City',20),
						  ('Phill Foden', 47, 'Manchester City',20),
						  ('Bernado Silva', 20,'Manchester City',20 )

Select Jogador.id_jogador, Jogador.nome_jogador from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 20



--insert do Chelsea
INSERT INTO Jogador VALUES('Edouard Mendy', 16, 'Chelsea',15),
						  ('Reece James', 24, 'Chelsea',15),
						  ('Andreas Christensen',4, 'Chelsea',15),
						  ('Antonio Rüdiger', 2, 'Chelsea',15),
						  ('César Azpilicueta', 28, 'Chelsea',15),
						  ('Jorginho', 5, 'Chelsea',15),
						  ('Mateo Kovačić', 8, 'Chelsea',15),
						  ('Marcos Alonso', 3, 'Chelsea',15),
						  ('Mason Mount', 19, 'Chelsea',15),
						  ('Kai Havertz', 29, 'Chelsea',15),
						  ('Time Werner', 11,'Chelsea',15 )

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 15


--insert do Barcelona
INSERT INTO Jogador VALUES('Marc Stegen ',1, 'Barcelona',16),
						  ('Dani Alves', 8, 'Barcelona',16),
						  ('Ronald Araujo',4, 'Barcelona',16),
						  ('Gerard Piqué',3, 'Barcelona',16),
						  ('Jordi Alba',18, 'Barcelona',16),
						  ('Frenkie de Jong', 21, 'Barcelona',16),
						  ('Sergio Busquets', 5, 'Barcelona',16),
						  ('Marcos Alonso', 3, 'Barcelona',16),
						  ('Ferran Torres', 19, 'Barcelona',16),
						  ('Pierre Aubameyang', 25, 'Barcelona',16),
						  ('Memphis Depay', 9,'Barcelona',16 )

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 16


--insert do Juventus
INSERT INTO Jogador VALUES('Marc Stegen ',1, 'Juventus',18),
						  ('Dani Alves', 8, 'Juventus',18),
						  ('Ronald Araujo',4, 'Juventus',18),
						  ('Gerard Piqué',3, 'Juventus',18),
						  ('Jordi Alba',18, 'Juventus',18),
						  ('Frenkie de Jong',4,'Juventus',18),
						  ('Sergio Busquets', 5, 'Juventus',18),
						  ('Marcos Alonso', 3, 'Juventus',18),
						  ('Ferran Torres', 19, 'Juventus',18),
						  ('Pierre Aubameyang', 25, 'Juventus',18),
						  ('Memphis Depay', 9,'Juventus',18)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 18

select * from Jogador

--insert do Liverpool
INSERT INTO Jogador VALUES('L. Díaz', 23, 'Liverpool',19),
						  ('S.mané', 10, 'Liverpool',19),
						  ('Mohamed Salah', 11, 'Liverpool',19),
						  ('Thiago', 6, 'Liverpool',19),
						  ('Fabinho', 3, 'Liverpool',19),
						  ('K. Keita', 8,'Liverpool',19),
						  ('A. Robertson', 26, 'Liverpool',19),
						  ('V. van Dijk', 4, 'Liverpool',19),
						  ('I. Konaté', 5, 'Liverpool',19),
						  ('T. Alexander Arnold', 66, 'Liverpool',19),
						  ('Alisson', 1,'Liverpool',19)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 19


--insert do Manchester United
INSERT INTO Jogador VALUES('D. de Gea', 1, 'Manchester United',21),
						  ('Diogo Dalot', 20, 'Manchester United',21),
						  ('H.Maguire', 5, 'Manchester United',21),
						  ('V.Lindelöf', 2, 'Manchester United',21),
						  ('B.Fernandes', 18, 'Manchester United',21),
						  ('P.Pogba', 6,'Manchester United',21),
						  ('A.Telles', 27, 'Manchester United',21),
						  ('J.Lingard', 14, 'Manchester United',21),
						  ('A.Elanga', 36, 'Manchester United',21),
						  ('C. Ronaldo', 7, 'Manchester United',21),
						  ('J.Sancho', 25,'Manchester United',21)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 21


--insert do Real Madrid
INSERT INTO Jogador VALUES('T.Courtois', 1, 'Real Madrid',22),
						  ('Carvajal', 2, 'Real Madrid',22),
						  ('Éder Militão', 3, 'Real Madrid',22),
						  ('Nacho', 6, 'Real Madrid',22),
						  ('F.Mendy', 23, 'Real Madrid',22),
						  ('Casemiro', 14, 'Real Madrid',22),
						  ('T. Kroos', 8,'Real Madrid',22),
						  ('L.Modrić',10, 'Real Madrid',22),
						  ('C. Ronaldo', 7, 'Real Madrid',22),
						  ('Vinícius Júnior', 20,'Real Madrid',22),
						  ('K. Benzema', 9,'Real Madrid',22)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 22


--insert do Tottenham
INSERT INTO Jogador VALUES('H.Lloris', 1, 'Tottenham',23),
						  ('C. Romero', 4, 'Tottenham',23),
						  ('E. Dier', 15, 'Tottenham',23),
						  ('B. Davies', 33, 'Tottenham',23),
						  ('Emerson', 12, 'Tottenham',23),
						  ('D. Kulusevskj', 21, 'Tottenham',23),
						  ('R. Bentancur', 30,'Tottenham',23),
						  ('P. Højbjerg', 5, 'Tottenham',23),
						  ('Reguilón', 3, 'Tottenham',23),
						  ('H. Kane', 10,'Tottenham',23),
						  ('H. Son',7,'Tottenham',23)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 23

Select * from Jogador

--inserir jogadores da liga portuguesa


--insert no Porto
INSERT INTO Jogador VALUES('Diogo Costa', 99, 'Porto',43),
						  ('Pepê', 11, 'Porto',43),
						  ('C. Mbemba', 19, 'Porto',43),
						  ('Pepe', 3, 'Porto',43),
						  ('Emerson', 12, 'Porto',43),
						  ('Z. Sanusi', 12, 'Porto',43),
						  ('Otávio', 25,'Porto',43),
						  ('M. Grujic', 16, 'Porto',43),
						  ('M. Taremi' ,9, 'Porto',43),
						  ('Fábio Vieira', 50,'Porto',43),
						  ('Evanilson', 30,'Porto',43)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 43

--insert no Benfica
INSERT INTO Jogador VALUES('D. Núñez', 9, 'Benfica',44),
						  ('D. Gonçalves', 17, 'Benfica',44),
						  ('G. Ramos', 88, 'Benfica',44),
						  ('Everton', 7, 'Benfica',44),
						  ('J. Weigl', 28, 'Benfica',44),
						  ('A. Taarabt', 49, 'Benfica',44),
						  ('Álex Grimaldo', 3,'Benfica',44),
						  ('J.Vertonghen', 5, 'Benfica',44),
						  ('N. Otamendi', 30, 'Benfica',44),
						  ('Gilberto', 2,'Benfica',44),
						  ('O. Vlachodimos', 99,'Benfica',44)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 44


--insert no Sporting
INSERT INTO Jogador VALUES('Adán', 1, 'Sporting',45),
						  ('L. Neto', 13, 'Sporting',45),
						  ('S. Coates', 4, 'Sporting',45),
						  ('Gonçalo Inácio', 25, 'Sporting',45),
						  ('Pedro Porro', 24, 'Sporting',45),
						  ('J. Palhinha', 6, 'Sporting',45),
						  ('Matheus Luiz', 8, 'Sporting',45),
						  ('J.Vertonghen', 5, 'Sporting',45),
						  ('N. Santos', 11, 'Sporting',45),
						  ('Pablo Sarabia', 17,'Sporting',45),
						  ('Pedro Gonçalves', 28, 'Sporting',45)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 45


--insert no Braga
INSERT INTO Jogador VALUES('R. Horta', 21, 'Braga',46),
						  ('Abel Ruiz', 9, 'Braga',46),
						  ('R. Gomes', 57, 'Braga',46),
						  ('A. Horta', 10, 'Braga',46),
						  ('Al-Musrati', 8, 'Braga',46),
						  ('Castro', 88, 'Braga',46),
						  ('Fabiano', 70, 'Braga',46),
						  ('V. Tormena', 3, 'Braga',46),
						  ('D. Carmo', 16, 'Braga',46),
						  ('P. Oliveira', 15,'Braga',46),
						  ('Matheus', 1, 'Braga',46)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 46



--insert no Gil Vicente
INSERT INTO Jogador VALUES('Z. Frelih', 1, 'Gil Vicente',47),
						  ('Z. C', 2, 'Gil Vicente',47),
						  ('Lucas Cunha', 3, 'Gil Vicente',47),
						  ('R. Fernades', 26, 'Gil Vicente',47),
						  ('Talocha', 31, 'Gil Vicente',47),
						  ('Pedrinho', 8, 'Gil Vicente',47),
						  ('Carvalho', 21, 'Gil Vicente',47),
						  ('A. Léautey', 11, 'Gil Vicente',47),
						  ('K. Fujimoto', 10, 'Gil Vicente',47),
						  ('S. Lino', 29, 'Gil Vicente',47),
						  ('Fran Navarro', 9, 'Gil Vicente',47)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 47

--insert no Vitoria SC
INSERT INTO Jogador VALUES('B. Varela', 14, 'Vitoria SC',48),
						  ('M. Maga', 62, 'Vitoria SC',48),
						  ('J. Fernandes', 44, 'Vitoria SC',48),
						  ('A. Mumin', 6, 'Vitoria SC',48),
						  ('Rafa', 5, 'Vitoria SC',48),
						  ('André Almeida', 70, 'Vitoria SC',48),
						  ('I. Bamba', 41, 'Vitoria SC',48),
						  ('N. Janvier', 98, 'Vitoria SC',48),
						  ('Ruben Lambeiras', 8, 'Vitoria SC',48),
						  ('O. Estupiñán', 19, 'Vitoria SC',48),
						  ('Rochinha', 16, 'Vitoria SC',48)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 48


--insert no Maritimo
INSERT INTO Jogador VALUES('Joel', 95, 'Maritimo',50),
						  ('A. Vídigal', 7, 'Maritimo',50),
						  ('M. Ferreira de Sousa', 36, 'Maritimo',50),
						  ('R. Guitane', 8, 'Maritimo',50),
						  ('P. Pelágio', 60, 'Maritimo',50),
						  ('I. Rossi', 15, 'Maritimo',50),
						  ('Vítor', 94, 'Maritimo',50),
						  ('Z. Júnior', 5, 'Maritimo',50),
						  ('M. Costa', 4, 'Maritimo',50),
						  ('C. Winck', 2, 'Maritimo',50),
						  ('P. Víctor', 48, 'Maritimo',50)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogador.id_clube=Clube.id_clube and Jogador.id_clube = 50

INSERT INTO Jogador VALUES('André Ferreira', 13, 'P. Ferreira',49),
						  ('J. Delgado', 15, 'P. Ferreira',49),
						  ('N. Lima', 3, 'P. Ferreira',49),
						  ('Maracás', 6, 'P. Ferreira',49),
						  ('L. Bastos', 20, 'P. Ferreira',49),
						  ('L. Carlos', 22, 'P. Ferreira',49),
						  ('R. Pires', 26, 'P. Ferreira',49),
						  ('Nuno Santos', 77, 'P. Ferreira',49),
						  ('Hélder Ferreira', 7, 'P. Ferreira',49),
						  ('A. Butzke', 17, 'P. Ferreira',49),
						  ('Zé Uilton', 9, 'P. Ferreira',49)

Select Jogador.id_jogador, Jogador.nome_jogador 
from Jogador inner join Clube on Jogado.id_clube=Clube.id_clube and Jogador.id_clube = 48

INSERT INTO Eventos_jogo VALUES(5,0,0,40,5,5,2,35,'5-0', '5-0',13,14)

INSERT INTO Jornada VALUES(1,3),
						  (2,4),
						  (3,4),

select * from Jornada
select * from Clube
select * from Eventos_jogo